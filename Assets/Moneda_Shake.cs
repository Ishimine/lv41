﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moneda_Shake : MonoBehaviour {


    [SerializeField]
    public ShakeConfig shakeDebil;
    [SerializeField]
    public ShakeConfig shakeFuerte;

    IEnumerator rutina;


    // .7 5 .2
    

    public void IniciarShake(ShakeConfig x)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Shake(x);
        StartCoroutine(rutina);
    }


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            if (rutina != null) StopCoroutine(rutina);
            rutina = Shake(shakeDebil);
            StartCoroutine(rutina);
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            if (rutina != null) StopCoroutine(rutina);
            rutina = Shake(shakeFuerte);
            StartCoroutine(rutina);
        }
    }

    IEnumerator Shake(ShakeConfig c)
    {
        float d = c.duracion;
        float t = Mathf.PI;
        Vector3 dif = new Vector3(Mathf.Sin(t * c.frecuencia) * c.magnitud, transform.position.y) - transform.position;
        do
        {
            t -= (Time.fixedDeltaTime * Mathf.PI) / d;    
            transform.position = new Vector3(Mathf.Sin(t * c.frecuencia) * c.magnitud,transform.position.y) - dif;
            yield return new WaitForFixedUpdate();
        } while (t>0);
    }

    [System.Serializable]
    public class ShakeConfig
    {
        public float duracion;
        public int frecuencia;
        public float magnitud;
        public ShakeConfig(float duracion, int frecuencia, float magnitud)
        {
            this.duracion = duracion;
            this.frecuencia = frecuencia;
            this.magnitud = magnitud;
        }

        /// <summary>
        /// Lerp entre 2 configuraciones
        /// </summary>
        /// <returns></returns>
        public static ShakeConfig Lerp(ShakeConfig a, ShakeConfig b, float t)
        {
            ShakeConfig n = new ShakeConfig(Mathf.Lerp(a.duracion, b.duracion, t),
                                            (int)Mathf.Lerp(a.frecuencia, b.frecuencia, t),
                                            Mathf.Lerp(a.magnitud, b.magnitud, t));
            return n;
        }
    }




}
