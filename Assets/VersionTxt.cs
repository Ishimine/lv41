﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VersionTxt : MonoBehaviour {

    TextMesh txt;

    private void Awake()
    {
        txt = GetComponent<TextMesh>();
        txt.text = "v "+Application.version;
    }
}
