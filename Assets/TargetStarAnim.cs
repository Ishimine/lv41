﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetStarAnim : MonoBehaviour {

    public SpriteRenderer renderA;
    public SpriteRenderer renderB;
    /// <summary>
    /// Tiempo que dura la animacion In y Out en segundos
    /// </summary>
    public float tIn = .1f;
    public float tOut = .3f;
    public float tPausa = .15f;
    private int act = 2;

    IEnumerator rutina;

    private void Start()
    {
    }

    public void Iniciar()
    {
        gameObject.SetActive(true);
        rutina = In();
        StartCoroutine(rutina);
    }
    public IEnumerator animacion()
    {
        while(true)
        {
           
                renderA.color = new Color(renderA.color.r, renderA.color.g, renderA.color.b, Mathf.PingPong((Time.time + .5f) / tOut, 1));
                renderB.color = new Color(renderB.color.r, renderB.color.g, renderB.color.b, Mathf.PingPong(Time.time / tOut, 1));
            

                     
            yield return null;
        }
    }

    public IEnumerator In()
    {
        float x = 0;
        Color a = new Color(renderA.color.r, renderA.color.g, renderA.color.b, 1);
        renderA.color = a;
        renderB.color = a;

        float l = 0;
        float dimInicial = transform.localScale.x;
        float dimMaxima = 1.2f;

        while (x < 1)
        {
            if (Time.timeScale == 0)
            {
                yield return null;
            }
            else
            {

                x += Time.deltaTime / tIn;

                if (x < .5)
                {
                    l = Mathf.Lerp(0, dimMaxima, x * 2);
                    transform.localScale = new Vector3(l, l, l);
                }
                else
                {
                    l = Mathf.Lerp(dimMaxima, dimInicial, (x - .5f)*2);
                    transform.localScale = new Vector3(l, l, l);

                }
            }
            yield return null;
        }

        Color c = new Color(renderA.color.r, renderA.color.g, renderA.color.b, 1);
        renderA.color = c;
        renderB.color = c;

        yield return new WaitForSeconds(tPausa);
        rutina = Out();
        StartCoroutine(rutina);
    }

    public IEnumerator Out()
    {
        float x = 0;
        while (x < 1)
        {
            yield return null;
            Color c = new Color(renderA.color.r, renderA.color.g, renderA.color.b, Mathf.Lerp(1, 0, x));
            renderA.color = c;
            renderB.color = c;
            x += Time.deltaTime / tOut;
        }
        gameObject.SetActive(false);
    }


    private void OnDestroy()
    {
       if(rutina != null) StopCoroutine(rutina);
    }
}
