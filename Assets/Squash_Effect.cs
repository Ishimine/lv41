﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Squash_Effect : MonoBehaviour {

    public float dimX;
    public float dimY;
    public GameObject cPadre;
    public GameObject cHijo;
    public float vel = 1;

    IEnumerator rutina;


    private float dimHijoOrig;

    private void Awake()
    {
        dimHijoOrig = cHijo.transform.localScale.x;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (rutina != null)
        {
            StopCoroutine(rutina);
           // cPadre.transform.localScale = Vector3.one;
          //  cHijo.transform.localScale = Vector3.one * dimHijoOrig;
        }
        rutina = Squash(collision.contacts[0].point);
        StartCoroutine(rutina);
        
    }


  

    IEnumerator Squash(Vector3 p)
    {
        float t = 1;
        do
        {
            t -= Time.deltaTime/vel;
            Vector2 dif = cPadre.transform.position - p;
            float rot = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;
            cPadre.transform.rotation = Quaternion.Euler(cPadre.transform.rotation.x, cPadre.transform.rotation.y, rot);
            cHijo.transform.rotation = Quaternion.identity;


            float y = Mathf.Sin(t*2) * dimX + 1;

            float x = Mathf.Sin(t * 2) * dimY + 1;
            cPadre.transform.localScale = new Vector3(x, y);

            yield return new WaitForEndOfFrame();
        } while (t > 0);
        cPadre.transform.localScale = new Vector3(1, 1);
    }
}
