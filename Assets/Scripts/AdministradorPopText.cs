﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdministradorPopText : MonoBehaviour {


    public bool activado;
    public int cant;
    public PopText[] textos;
    public int tActual;
    public static  AdministradorPopText instance;


    public void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }


        textos = new PopText[cant];
        for (int i = 0; i < cant; i++)
        {
            GameObject clone = Instantiate(Resources.Load("PickUp/PopText", typeof(GameObject)), this.transform) as GameObject;
            clone.transform.position = new Vector3(0, 0, -50);
            textos[i] = clone.GetComponent<PopText>();
        }
    }

    public void SetTexto(string s)
    {
        if (!activado) return;
        textos[tActual].SetText(s);
    }

    public void SetColor(Color c)
    {
        if (!activado) return;
        textos[tActual].SetColor(c);
    }
    public void SetPos(Vector3 pos)
    {
        if (!activado) return;
        textos[tActual].transform.position = pos; 
    }

    public void Activar()
    {
        if (!activado) return;
        textos[tActual].Activar();
        tActual++;

        if (tActual >= 5) tActual = 0;

        //Debug.Log(tActual % (cant - 1));

    }


}
