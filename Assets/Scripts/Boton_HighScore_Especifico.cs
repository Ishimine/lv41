﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton_HighScore_Especifico : BotonBase {



	public override void BotonApretado()
    {
        switch (GameController.instance.modoDeJuegoActivo)
        {
            case GameController.modo.Menu:
                break;
            case GameController.modo.Survival_Classic:
                PlayServicesControl.ShowLeaderBoard(GPGSIds.leaderboard_classic);
                break;
            case GameController.modo.Survival_Restrained:
                PlayServicesControl.ShowLeaderBoard(GPGSIds.leaderboard_limited);
                break;
            case GameController.modo.Survival_Spike:
                PlayServicesControl.ShowLeaderBoard(GPGSIds.leaderboard_spikes);
                break;
            case GameController.modo.Survival_TimeBomb:
                PlayServicesControl.ShowLeaderBoard(GPGSIds.leaderboard_time_bomb);
                break;
            case GameController.modo.Survival_Rain:
                break;
            case GameController.modo.Survival_MoneyHunter:
                break;
            case GameController.modo.Survival_Twister:
                break;
            case GameController.modo.Niveles:
                break;
            default:
                break;
        }
    }



}
