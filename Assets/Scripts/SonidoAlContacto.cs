﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoAlContacto : MonoBehaviour {

    public bool variarPitch = false;
    public float rango = 0.2f;
    public float centro = 1f;
    public AudioSource source;

    private void Awake()
    {
        if(source == null)
        {
            source = GetComponent<AudioSource>();
        }
    }   

    virtual public void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(variarPitch)
            {
                source.pitch = Random.Range(centro - rango, centro + rango);
            }
            source.Play();
        }
    }
}
