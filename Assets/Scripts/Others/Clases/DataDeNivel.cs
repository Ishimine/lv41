﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataDeNivel {

    [SerializeField]
    public int idNivel;
    [SerializeField]
    public float[] puntajes = new float[3];

    public DataDeNivel ()
    {
        idNivel = -1;
        puntajes[0] = 9999;
        puntajes[1] = 9999;
        puntajes[2] = 9999;
    }

    public DataDeNivel(int i, float puntaje_0, float puntaje_1, float puntaje_2)
    {
        idNivel = i;
        puntajes[0] = puntaje_0;
        puntajes[1] = puntaje_1;
        puntajes[2] = puntaje_2;
    }
    
}
