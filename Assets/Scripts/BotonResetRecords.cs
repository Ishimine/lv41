﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonResetRecords : MonoBehaviour {

	public void ResetRecords()
    {
        AdministradorPuntaje.instance.ResetRecords();
    }
}
