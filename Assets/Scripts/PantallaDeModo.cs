﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PantallaDeModo : MonoBehaviour {

    public Image candado;
    public Button play;
    public GameController.modo modo;


    public void Start()
    {
        ChequearEstado();
    }
    
    public void ChequearEstado()
    {
        SetEstado(AdministradorDeModos.GetEstado(modo));
    }

    public void SetEstado(bool x)
    {
        if(x)
        {
            play.gameObject.SetActive(true);
            candado.gameObject.SetActive(false);
            play.interactable = true;
        }
        else
        {
            play.gameObject.SetActive(false);
            candado.gameObject.SetActive(true);
            play.interactable = false;
        }
    }


}
