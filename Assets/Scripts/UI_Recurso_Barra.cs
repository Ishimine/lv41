﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Recurso_Barra : MonoBehaviour {


    public Slider sliderInf;
    public Slider sliderMed;
    public Slider sliderSup;



    public bool actMed = false;
    //public bool actSup = false;
    float sliderMedObj;
    public static UI_Recurso_Barra instance;

    public float smooth = 0.4f;
    private float vel;
    private float vel2;

    private void Awake()
    {        
      //  Debug.Log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa");  
        Enlazar();
    }

    public void Enlazar()
    {
        TouchControlV2.instance.recurso.actSumar -= Sumar;
        TouchControlV2.instance.recurso.actPrevisualizar -= Previsualizar;
        TouchControlV2.instance.recurso.actRestar -= Restar;

        TouchControlV2.instance.recurso.actSumar += Sumar;
        TouchControlV2.instance.recurso.actPrevisualizar += Previsualizar;
        TouchControlV2.instance.recurso.actRestar += Restar;
    }

    
 
    public void Llenar()
    {
        sliderInf.value = 1;
        sliderSup.value = 1;
        sliderMed.value = 1;
    }
    

    public void Previsualizar(float x)
    {
        float n = x / TouchControlV2.instance.config.LongitudMax;
        sliderSup.value = n;
       // Debug.Log("Previsualizar " + n);
    }

    public void Restar(float x)
    {
        float n = x / TouchControlV2.instance.config.LongitudMax;
        sliderInf.value = n;
        sliderSup.value = n;
     //   Debug.Log("Restar " + n);
    }

    public void Sumar(float x)
    {
        float n = x / TouchControlV2.instance.config.LongitudMax;     // Mathf.Lerp(TouchControl.instance.longBarraMin, TouchControl.instance.cantBarraMax, TouchControl.instance.CantBarraActual);

        sliderMedObj = sliderSup.value;
        sliderMed.value = sliderSup.value;


        sliderInf.value = n;
    //    Debug.Log("Sumar " + n);
    }

    public void Update()
    {
        if (Time.timeScale == 0) return;

        sliderSup.value = Mathf.SmoothDamp(sliderSup.value, sliderInf.value, ref vel, smooth);

        if (actMed)
        {
            sliderMed.value = Mathf.SmoothDamp(sliderMed.value, sliderMedObj, ref vel2, smooth);
            if ( Mathf.Abs(sliderMed.value - sliderMedObj) < 0.2f ) actMed = false;
        }
        else
        {
            sliderMed.value = Mathf.SmoothDamp(sliderMed.value, sliderInf.value, ref vel2, smooth);
        }
    }

    private void OnDestroy()
    {
       // Debug.Log("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDdd");
      //  GameController.PreSesionDeJuego -= Enlazar;
        TouchControlV2.instance.recurso.actSumar -= Sumar;
        TouchControlV2.instance.recurso.actPrevisualizar -= Previsualizar;
        TouchControlV2.instance.recurso.actRestar -= Restar;
    }
}
