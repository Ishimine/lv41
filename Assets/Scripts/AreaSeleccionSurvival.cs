﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaSeleccionSurvival : AreaDeSeleccion {

    public GameController.modo modo;


    public override void Accion()
    {
        GameController.CambiarModoDeJuego(modo);
    }
}
