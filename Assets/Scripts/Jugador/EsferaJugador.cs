﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsferaJugador : MonoBehaviour {

    /// <summary>
    /// Si es False, solo rebotara contra objetos que tengan el componente PropiedadesMat. Caso contrario con objetos sin Propiedades Mat
    /// rebotara igual al fRebote
    /// </summary>
    public Vector3 lastMove;
    public SpriteRenderer render;
    public SonidosPorArreglo audios;
    public CuentaRegresiva cuenta;
    public bool animar = false;
    public bool rebotarSiempre;
    public bool usarSistViejo;
    public bool usarUnicoContacto;

    public GameObject trail;
    
    public Collider2D col;
    public Rigidbody2D rb;



    public delegate void gatillo();
    public event gatillo muerto;

    public ControlParticula particulasImpacto;
    public ControlParticula particulasMuerte;
    public ControlParticula particulasMoneda;
    public int cantParticulas = 30;

    public float fRebote = 10;

    public Animator anim;


    public bool dead = false;

    IEnumerator rutina;

    public void desactivarTrail()
    {
        trail.SetActive(false);
    }
    public void activarTrail()
    {
        trail.SetActive(true);
    }

    

    private void Start()
    {
        EsferaDesactivada();
        InicializarJugador();
        dead = false;
    }

    private void OnDestroy()
    {
        if (rutina != null) StopCoroutine(rutina);
        GameController.InicioSesionDeJuego -= ActivarEsfera;

    }

    void InicializarJugador()
    {
        if (anim != null) anim.SetBool("Vivo",true);

        GameController.InicioSesionDeJuego += ActivarEsfera;
        //trail.SetActive(true);

        //Aplicar Skin
        //ReiniciarVida
    }

    public void RevivirJugador()
    {
        col.isTrigger = false;
        rb.simulated = true;
        rb.isKinematic = false;
        dead = false;
        if (anim != null) anim.SetBool("Vivo", true);
    }

    public void EnMeta()
    {
        deshabilitar();
        if (anim != null && animar)
            anim.SetTrigger("Meta");
    }

    public void deshabilitar()
    {
        //print("Desabilitada");
        rb.velocity = Vector2.zero;
        rb.simulated = false;
        col.isTrigger = true;

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!this.isActiveAndEnabled)
        {
            return;
        }
        if (usarSistViejo)
        {
            Vector2 dir = (Vector2)transform.position - other.contacts[0].point;
            rb.velocity = dir * fRebote;
        }
        else if (!usarUnicoContacto)
        {
            foreach (ContactPoint2D cont in other.contacts)
            {
                Vector2 contact = cont.point;
                Vector2 dir = ((Vector2)transform.position - contact).normalized;

                if (other.gameObject.GetComponent<PropiedadesMat>() != null)
                {
                    rb.AddForce(dir * other.gameObject.GetComponent<PropiedadesMat>().indiceRebote * fRebote, ForceMode2D.Impulse);
                }
                else if (rebotarSiempre)
                    rb.AddForce(dir * fRebote, ForceMode2D.Impulse);
            }
        }
        else
        {
            Vector2 dir = ((Vector2)transform.position - other.contacts[0].point).normalized;
            if (other.gameObject.GetComponent<PropiedadesMat>() != null)
            {
                rb.AddForce(dir * other.gameObject.GetComponent<PropiedadesMat>().indiceRebote * fRebote, ForceMode2D.Impulse);
            }
            else if (rebotarSiempre)
                rb.AddForce(dir * fRebote, ForceMode2D.Impulse);
        }

        if (anim != null && animar) anim.SetTrigger("Rebote");
        //ShakeControl.instance.ActivarShake(ShakeControl.FuerzaShake.Debil);


        PropiedadesMat ma = other.gameObject.GetComponent<PropiedadesMat>();
        Color c = render.color;
        if (ma != null) c = ma.c;
        BurstParticulasChico(other.contacts[0].point, c, cantParticulas);

        /*
        PropiedadesMat ma = other.gameObject.GetComponent<PropiedadesMat>();
        particulasImpacto.transform.position = other.contacts[0].point;
        if (ma != null) particulasImpacto.CambiarColorInicial(ma.c);
        particulasImpacto.CrearBurst(cantParticulas);
        */
    }

    public void BurstParticulasChico(Vector3 pos, Color c, int cant)
    {
        particulasImpacto.transform.position = pos;
        particulasImpacto.CambiarColorInicial(c);
        particulasImpacto.CrearBurst(cant);
    }

    public void BurstParticulasGrande(Vector3 pos,int cant)
    {
        particulasMoneda.transform.position = pos;
        particulasMoneda.CrearBurst(cant);
    }

    public void BurstParticulasGrande(Vector3 pos, Color c, int cant)
    {
        particulasMoneda.CambiarColorInicial(c);
        BurstParticulasGrande(pos, cant);
    }

    public void EsferaDesactivada()
    {
        trail.SetActive(false);
        rutina = Flotando();
        StartCoroutine(rutina);
    }

    public void ActivarEsfera()
    {
        trail.SetActive(true);

        if (rutina != null) StopCoroutine(rutina);
        
    }

    IEnumerator Flotando()
    {
        while (true)
        {
            Vector3 a = Vector3.up * (Mathf.Sin(Time.realtimeSinceStartup * 13) / 25);
            transform.position = transform.position - lastMove + a;
            lastMove = a;

            //transform.position = transform.position + Vector3.up * (Mathf.Sin(Time.realtimeSinceStartup*10)/30);
            yield return null;
        }
    }


    public void Dead()
    {
        if (muerto != null)
        {            
            //if (anim != null && animar) anim.SetTrigger("Muerto");
            Debug.Log("Jugador DEAD");
            GameController.instance.juego.jugador = null;
            muerto();
            Destroy(gameObject);
        }
    }

    public void CambiarColor()
    {

    }

    public void MuerteExplosiva()
    {
        if (dead) return;
        if (anim != null)
        {
            anim.SetTrigger("Muerte Explosiva");
            anim.SetBool("Vivo", false);
            //rb.gravityScale = 0;
        }
        if (particulasMuerte != null)
        {
            particulasMuerte.CambiarColorInicial(Recursos.instance.setColorActual.color[2].tono);
            particulasMuerte.transform.position = transform.position;
            particulasMuerte.CrearBurst(50);
        }

        if (audios != null)
        {
            audios.ReproducirAudio(0, 0);
            audios.ReproducirAudio(1, 1);
        }

        ShakeControl.instance.ActivarShake(ShakeControl.FuerzaShake.Medio);
        rb.simulated = false;
        rb.isKinematic = false;
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0;
        dead = true;
        trail.SetActive(false);

        Handheld.Vibrate();
        StartCoroutine( Esperar(.5f));
    }

    
    IEnumerator Esperar(float x)
    {
        yield return new WaitForSeconds(x);
        Dead();
    }

}
