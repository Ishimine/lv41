﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RequisitosDeModo {

    public string nombreOficial;
    public GameController.modo modoReferencia;
    public bool desbloqueado = false;
    public float objetivo = 50;
    public string descripcionObjetivo = "";

    public RequisitosDeModo()
    {
        desbloqueado = false;
    }

    public bool DesbloquearModo(float x)
    {
        if (x >= objetivo)
        {
            desbloqueado = true;
        }
        return desbloqueado;
    }

}
