﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameMode_Survival_Spike : GameMode_Survival, ModoConPanelesMonedas
{
    public int valorInterruptores;             
    public int valorInterruptoresDorados;     

    static Escenario_Survival_Spike esc_spike;

    public void PanelMonedaAtrapado()
    {
    }

    public void PanelMonedaPerdido()
    {
    }

    public void PanelMonedaEspecialAtrapado()
    {
    }

    public void PanelMonedaEspecialPerdido()
    {
    }
}
