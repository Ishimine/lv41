﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class GameMode_Survival_Classic : GameMode_Survival, ModoConMonedas {

    public CreadorDeMonedas creadorDeMonedas;
    public int penalizacionPorBarra;
    public int penalizacionDeMonedaPerdida;
    public int tiempoPorMoneda;
    public float tiempoEntreMonedas;
    
    public override void InstanciacionGeneral()
    {
        base.InstanciacionGeneral();
        CrearContadorInGame();
        InstanciarCreadorDeMonedas();
    }

    public override void AplicarConfiguracion()
    {
        base.AplicarConfiguracion();
        if (contadorInGame != null) contadorInGame.gameObject.SetActive(true);
        if (creadorDeMonedas != null)
        {
            creadorDeMonedas.UsarMonedasConTiempo(configModo.monedasConTiempo);
            creadorDeMonedas.SetTiempoPorMoneda((int)configModo.tiempoMoneda);
            creadorDeMonedas.TiempoEntreMonedas = configModo.tiempoMoneda;
            creadorDeMonedas.Resetear();
        }       
    }

    public override void InicioSesionDeJuego()
    {
        base.InicioSesionDeJuego();
    }    


    public override void Desenlazar()
    {
        AdministradorPuntaje.instance.actPuntaje -= ActualizarTxt;
        TouchControlV2.barraCreada -= BarraCreada;
        contadorInGame.transform.SetParent(canvasActual.transform);
        base.Desenlazar();               
    }    

    public virtual void MonedaAtrapada()
    {
        SumarPuntaje(0, configModo.valorMoneda);
        TouchControlV2.instance.recurso.Recargar(2f);
        if (creadorDeMonedas != null) creadorDeMonedas.MonedaAtrapada();
    }

    public virtual void MonedaPerdida()
    {
        SumarPuntaje(0, penalizacionDeMonedaPerdida);
        if (creadorDeMonedas != null) creadorDeMonedas.MonedaPerdida();
    }

    public override void BarraPerdida()
    {
     
    }


    
    public override void FinSesionDeJuego()
    {
        base.FinSesionDeJuego();
        if(creadorDeMonedas != null) creadorDeMonedas.DetenerCreacionMoneds();
        if (contadorInGame!= null)  contadorInGame.gameObject.SetActive(false);
        if (ui_Recurso_Barra != null) ui_Recurso_Barra.Llenar();

    }




    public void InstanciarCreadorDeMonedas()
    {
        if (configModo.usarCreadorDeMonedas && creadorDeMonedas == null)
        {
            creadorDeMonedas = GameController.InstanciarGameObject("Prefabs/CreadorMonedas", null).GetComponent<CreadorDeMonedas>();
            creadorDeMonedas.usarMarcador = true;
        }
    }

    public override void JugadorMuerto()
    {
        Debug.Log("Jugador Muerto");
        FinSesionDeJuego();
    }

    public CreadorDeMonedas GetCreadorDeMonedas()
    {
        return creadorDeMonedas;
    }

    public virtual void MonedaEspecialAtrapada()
    {
    }

    public virtual void MonedaEspecialPerdida()
    {
    }
}
