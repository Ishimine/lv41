﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class GameMode_Survival : GameMode
{
    [HideInInspector] public TextMesh contadorInGame;

    public override void InstanciacionGeneral()
    {
        base.InstanciacionGeneral();
        if (configModo.usarKillBox) GameController.InstanciarGameObject("Prefabs/Killbox", Vector3.zero,Quaternion.identity);

        AdministradorPuntaje.instance.actPuntaje -= ActualizarTxt;
        AdministradorPuntaje.instance.actPuntaje += ActualizarTxt;
    }


    public override void AplicarConfiguracion()
    {
        base.AplicarConfiguracion();
        Time.timeScale = 0;
       // if(ui != null) ui.ActualizarEstado(1);
        if (jugador == null)
            CrearJugador();
        ReiniciarPuntaje();


        if (UI_Recurso_Barra.instance != null) UI_Recurso_Barra.instance.Sumar(TouchControlV2.instance.config.LongitudMax);


        if (contadorInGame == null)
        {
            contadorInGame = GameController.InstanciarGameObject("Canvas/ContadorInGame", null).GetComponent<TextMesh>();
        }
    }

    public override void CrearCanvas()
    {
        Debug.Log("CREADO CANVAS SURVIVAL");
        string s = "Canvas/Canvas_GameMode_Survival";
        canvasActual = GameController.InstanciarGameObject(s, GameController.instance.transform);
        ui = canvasActual.GetComponent<UI_Survival>();
        
    }

    public void ActualizarTxt(int id, float valor)
    {
        if (contadorInGame == null) return;
        if (valor == 0) contadorInGame.text = "";
        else contadorInGame.text = valor.ToString();
    }





    public override void CrearJugador()
    {
        base.CrearJugador();     
        jugador.GetComponent<EsferaJugador>().muerto += GameController.FinalizarSesionDeJuego;
        Wrap w = esferaJugador.GetComponent<Wrap>();
        w.activadoEnX = configModo.usarWrapX;
        w.activadoEnY = configModo.usarWrapY;
    }


    public override void Desenlazar()
    {
        base.Desenlazar();
        if (jugador != null) jugador.GetComponent<EsferaJugador>().muerto -= GameController.FinalizarSesionDeJuego;
    }






    public override void InicioSesionDeJuego()
    {
        Debug.Log("55555555" + ui);
        if(ui != null) ui.ActualizarEstado(1);
        Time.timeScale = 1;
        base.InicioSesionDeJuego();
    }

    public void CrearContadorInGame()
    {
        if (contadorInGame == null)
        {
            contadorInGame = GameController.InstanciarGameObject("Canvas/ContadorInGame", null).GetComponent<TextMesh>();
        }
    }
  
    public override void JugadorMuerto()
    {
        //
    }

    public override void JuegoPerdido()
    {
    }



    public virtual void ReiniciarPuntaje()
    {
        AdministradorPuntaje.instance.SetPuntaje(new DataDeNivel(1, 0, 0, 0));
    }

    public override void FinSesionDeJuego()
    {
       // Debug.Log("1111111");
        Debug.Log("FinSesionDeJuego en: " + this.GetType());
     // Debug.Log("1111111");
        TouchControlV2.instance.DeshabilitarBarras();
        if(ui!=null) ui.ActualizarEstado(4);
        GameController.enJuego = false;
        GameController.swipeActivo = false;

        AdministradorPuntaje.instance.CargarDatosDeNivel();
        DataDeNivel puntajeAct = AdministradorPuntaje.instance.GetPuntaje();
        DataDeNivel recordAct = AdministradorPuntaje.instance.GetRecord();
        if (ui != null) ui.MostrarPuntaje((int)puntajeAct.puntajes[0], (int)recordAct.puntajes[0]);
        AdministradorPuntaje.instance.GuardarPuntajeActual();

        GameController.Juego_AplicarConfiguracion();
        //GameController.instance.Juego_AplicarConfiguracion_Retrasada();
    }

    IEnumerator Esperar(float x)
    {
        yield return new WaitForSeconds(x);
        GameController.Juego_AplicarConfiguracion();

    }

    public override void BarraPerdida()
    {
    }

    public override void CierreDeEscena()
    {
    }
}
