﻿public interface IGameMode_Survival
{
    void AplicarConfiguracion();
    void BarraPerdida();
    void CrearCanvas();
    void CrearContadorInGame();
    void CrearJugador();
    void Desenlazar();
    void EventoNegativo(int id);
    void FinSesionDeJuego();
    void InicioSesionDeJuego();
    void InstanciacionGeneral();
    void JuegoPerdido();
    void JugadorMuerto();
    void ReiniciarPuntaje();
}