﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameMode_Menu : GameMode
{
    
    public override void AplicarConfiguracion()
    {
        base.AplicarConfiguracion();
        GameController.swipeActivo = true;
        GameController.enJuego = false;
        GameController.enPausa = false;
        if(ui2 != null) ui2.ActualizarEstado(0);
        Time.timeScale = 0;
        CrearJugador();
    }

    public override void Desenlazar()
    {        
        base.Desenlazar();
    }

    
    public override void JuegoPerdido()
    {
    }

    public override void JugadorMuerto()
    {
    }
    
    public override void BarraPerdida()
    {
    }
    
    public override void InicioSesionDeJuego()
    {
        Time.timeScale = 1;
        ui2.ActualizarEstado(1);

        //Debug.Log("Inicio sesion de juego: Menu");
    }

    public override void FinSesionDeJuego()
    {
    }


    public override void Pausa()
    {
    }

    public override void CierreDeEscena()
    {
    }
}
