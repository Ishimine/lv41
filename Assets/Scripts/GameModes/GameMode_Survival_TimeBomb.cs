﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameMode_Survival_TimeBomb : GameMode_Survival_Classic {

    float tiempoRestante;

    public void IniciarConfiguracion()
    {

    }

    public override void Update(float deltaTime)
    {
        base.Update();
        if(GameController.enJuego && !GameController.enPausa)
        {
            tiempoRestante -= deltaTime;
            if (tiempoRestante <= 0)
            {
                tiempoRestante = 0;
                jugador.GetComponent<EsferaJugador>().MuerteExplosiva();
            }

            SetPuntaje(0, tiempoDeJuego);
            SetPuntaje(2, tiempoRestante);
            if (contadorInGame != null)   contadorInGame.text = tiempoRestante.ToString("0.0");
        }
    }



    public override void Desenlazar()
    {
        base.Desenlazar();
        TouchControlV2.barraCreada -= BarraCreada;
        GameController.gcUpdate -= Update;
    }

    
    public override void MonedaAtrapada()
    {
        base.MonedaAtrapada();
        tiempoRestante += configModo.valorMoneda;
    }

    public override void MonedaPerdida()
    {
        base.MonedaPerdida();
    }


    public override void BarraPerdida()
    {
      
    }


    public override void AplicarConfiguracion()
    {
        base.AplicarConfiguracion();
        tiempoRestante = 30;

    }
    
    public override void FinSesionDeJuego()
    {
        base.FinSesionDeJuego();
        if(creadorDeMonedas != null) creadorDeMonedas.DetenerCreacionMoneds();
    }

}
