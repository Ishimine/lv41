﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class GameMode :  SesionDeJuego {

    public RequisitosDeModo requisitos;

    public ConfiguracionModo configModo;
    public ConfiguracionDeBarras configBarras;
    [HideInInspector] public float tiempoDeJuego;
    Transform pPartida;
    [HideInInspector] public GameObject canvasActual;
    [HideInInspector] public UI_GameMode ui;
    [HideInInspector] public UI_Base ui2;
    [HideInInspector] public UI_ComboMultiplicador ui_Combo;
    [HideInInspector] public UI_Recurso_Barra ui_Recurso_Barra;



    [SerializeField] public GameObject jugador;
    [SerializeField] public EsferaJugador esferaJugador;

    public GameMode()
    {
    }

    
    public virtual void CrearCanvasExtras()
    {
        if (AdministradorPuntaje.instance.UsarComboMultiplicador && ui_Combo == null)
        {
            ui_Combo = GameController.InstanciarGameObject("Canvas/UI_ComboMultiplicador", canvasActual.transform).GetComponent<UI_ComboMultiplicador>();
            ui_Combo.transform.SetAsFirstSibling();
        }

        if (TouchControlV2.instance.config.limitacion == ConfiguracionDeBarras.tipoLimitacion.RecursoSimple && ui_Recurso_Barra == null)
        {
            ui_Recurso_Barra = GameController.InstanciarGameObject("Canvas/UI_Recurso_Barra", canvasActual.transform).GetComponent<UI_Recurso_Barra>();
            ui_Recurso_Barra.transform.SetAsLastSibling();
        }
    }

    public virtual void Enlazar()
    {
        TouchControlV2.barraCreada -= BarraCreada;
        TouchControlV2.barraCreada += BarraCreada;


        GameController.InstanciacionGeneral += InstanciacionGeneral;
        GameController.AplicarConfiguracion += AplicarConfiguracion;
        GameController.InicioSesionDeJuego += InicioSesionDeJuego;
        GameController.FinSesionDeJuego += FinSesionDeJuego;
        GameController.CierreDeEscena += CierreDeEscena;

        GameController.gcUpdate += Update;
        GameController.gcFixedUpdate += FixedUpdate;
        TouchControlV2.barraCreada += BarraCreada;

    }

    public virtual void Update(float deltaTime)
    {
        if (GameController.enJuego && !GameController.enPausa)
        {
            tiempoDeJuego += deltaTime;
        }
    }

    public void SetPuntaje(int id, float valor)
    {
        AdministradorPuntaje.instance.SetPuntaje(id, valor);
    }


    public float SumarPuntaje(int id, float valor)
    {
        return AdministradorPuntaje.instance.SumarPuntaje(id, valor);
    }

    public virtual void Desenlazar()
    {
        GameController.InstanciacionGeneral -= InstanciacionGeneral;
        GameController.AplicarConfiguracion -= AplicarConfiguracion;
        GameController.InicioSesionDeJuego -= InicioSesionDeJuego;
        GameController.FinSesionDeJuego -= FinSesionDeJuego;
        GameController.CierreDeEscena -= CierreDeEscena;

        GameController.gcUpdate -= Update;
        GameController.gcFixedUpdate -= FixedUpdate;
        TouchControlV2.barraCreada -= BarraCreada;

        if (ui != null) ui.Desenlazar();
    }

    public virtual void BarraCreada(int i)
    {
        if ((!GameController.enJuego && i == 1))
            GameController.IniciarSesionDeJuego();
    }

    public virtual void CrearJugador()
    {
        // Debug.Log("Jugador Creado en " + this.GetType());
        jugador = GameController.InstanciarGameObject("Prefabs/Player", pPartida);
        esferaJugador = jugador.GetComponent<EsferaJugador>();
    }

    public virtual void BarraPerdida() { }

    public virtual void JugadorMuerto() { }

    public virtual void JuegoPerdido() { }

    public virtual void Pausa()
    {
        ui.Pausa(!GameController.enPausa);
    }

    public virtual void CrearCanvas()
    {
        Debug.Log("Canvas Creado"+ GetType().ToString()); 
        string s = "Canvas/Canvas_" + this.GetType();
        canvasActual = GameController.InstanciarGameObject(s, null);
        ui = canvasActual.GetComponent<UI_GameMode>();
        ui2 = canvasActual.GetComponent<UI_Base>();
    }

    public GameObject GetCanvasActual()
    {
        return canvasActual;
    }



    #region EVENTOS


    public virtual void InstanciacionGeneral()
    {
        TouchControlV2.instance.AplicarConfiguracion();
        TouchControlV2.barraCreada += BarraCreada;
        pPartida = GameObject.FindGameObjectWithTag("pPartida").transform;
        CrearCanvas();
        CrearCanvasExtras();
        GameController.instance.canvasActual = canvasActual;
    }



    public virtual void AplicarConfiguracion()
    {
        tiempoDeJuego = 0;
        GameController.swipeActivo = true;
        GameController.enJuego = false;
    }

    public virtual void InicioSesionDeJuego()
    {
        GameController.enJuego = true;
        if (ui != null) ui.ActualizarEstado(1);
    }

    public virtual void FinSesionDeJuego() { }
    public virtual void CierreDeEscena() { }

    public void PausaIn()
    {
    }

    public void PausaOut()
    {
    }







    public virtual void Update()
    {
        if (GameController.enJuego && !GameController.enPausa)
        {
            tiempoDeJuego += Time.deltaTime;
        }
    }

    public virtual void FixedUpdate(float x)
    {

    }

    #endregion



    [System.Serializable]
    public class ConfiguracionModo
    {

        public bool usarCreadorDeMonedas = true;
        public bool monedasConTiempo = true;
        public float valorMoneda = 1;
        public float tiempoMoneda = 8;
        [Space(1)]
        public bool usarKillBox = true;
        public bool usarWrapX = false;
        public bool usarWrapY = true;
        [Space(1)]
        public bool usarRecurso = true;
        public float valorRecarga = 10;
        [Space(1)]
        public bool usarComboMultiplicador;
        public int pasosPorMultiplicacion;
        public float intensidadDeMultiplicacion;
        public float multMaximo;
        public float tiempoDeEnfriamiento;

    }
}
