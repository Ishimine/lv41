﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Escenario_Survival_Spike : MonoBehaviour {


    public GameObject[] paredes;
    public GameObject prefab;
    public bool der;

    public int panelesPorPared = 5;

    private float alturaPanel = 1;
    public PanelSpike[,] panelesSpike;

    float posXPaneles;
    

    private void Awake()
    {
        IniciarJuego();
    }
    
    public void SetPanelesPorPared(int x)
    {
        panelesPorPared = x;
    }

    private void AplicarAltura()
    {
        foreach(PanelSpike panel in panelesSpike)
        {
            panel.transform.localScale = new Vector2(transform.localScale.x, alturaPanel);
        }
    }

    private void CalcularTamañoPorPanel()
    {
        alturaPanel = (Camera.main.orthographicSize / panelesPorPared * 2);
    }

    /// <summary>
    /// Codigo CROTO
    /// </summary>
    public void ActualizarPaneles(int x)
    {
        SetPanelesPorPared(x);  
        DestruirPaneles();
        CalcularTamañoPorPanel();
        CrearPaneles();
        ReRollParedes();
    }

    public void DestruirPaneles()
    {
        PanelSpike[] paneles = FindObjectsOfType<PanelSpike>();
        foreach (PanelSpike p in paneles)
        {
            p.StopAllCoroutines();
            Destroy(p.gameObject);
        }
    }

    public void PanelTocado()
    {
        //GameController.instance.juego.InterruptorApretado();
    }

    private void CrearPaneles()
    {
        panelesSpike = new PanelSpike[2, panelesPorPared];
        prefab.transform.localScale = new Vector3(2, alturaPanel);

        float posY = -Camera.main.orthographicSize + alturaPanel/2;

        float posX = Camera.main.orthographicSize / Screen.height * Screen.width;

        for (int i=0; i<panelesPorPared;i++)
        {
            GameObject panel = Instantiate<Object>(Resources.Load("Terreno/PanelSpike"), Vector3.zero, Quaternion.identity, transform) as GameObject;
            panelesSpike[0, i] = panel.GetComponent<PanelSpike>();
            panelesSpike[0, i].SetEscenario(this);
            panelesSpike[1, i] = (Instantiate<Object>(Resources.Load("Terreno/PanelSpike"), Vector3.zero, Quaternion.Euler(0, 0, 180), transform) as GameObject).GetComponent<PanelSpike>();
            panelesSpike[1, i].SetEscenario(this);
        }

        PosicionarParedes();
        PosicionarPaneles();
    }

    private void PosicionarPaneles()
    {
        float posX = Camera.main.orthographicSize / Screen.height * Screen.width;
        float posY = -Camera.main.orthographicSize + alturaPanel/2;
        for (int i =0;i<panelesPorPared;i++)
        {
            panelesSpike[0, i].transform.position = new Vector3(-posX, posY + (i * alturaPanel));
            panelesSpike[1, i].transform.position = new Vector3(posX, posY + (i * alturaPanel));
        }
    }

    private void PosicionarParedes()
    {
        float posX = Camera.main.orthographicSize / Screen.height * Screen.width;

        paredes[0].transform.position = new Vector2(posX+transform.localScale.x*2, 0);
        paredes[1].transform.position = new Vector2(-posX-transform.localScale.x*2, 0); 
    }    

    public void IniciarJuego()
    {
        ActualizarPaneles(1);
    }

    public void ReRollParedes()
    {
        if (der)
        {
            SetParedOfensivo(0);
            SetDefensivoAleatorio(0);
            SetParedOfensivo(1);
        }
        else
        {
            SetParedOfensivo(1);
            SetDefensivoAleatorio(1);
            SetParedOfensivo(0);
        }
    }

    public void Siguiente()
    {
        der = !der;
        ReRollParedes();
    }

    private void SetDefensivoAleatorio(int idPared)
    {
        int x = Random.Range(0, panelesSpike.GetLength(1));
        SetPanelOfensivo(idPared, x, false);
    }

    private void SetPanelActive(int c, int f, bool x)
    {
        panelesSpike[c, f].gameObject.SetActive(x);
    }

    private void SetPanelOfensivo(int c, int f, bool x)
    {
        if (x)
            panelesSpike[c, f].Ofensivo();
        else
        {
            if (panelesSpike[c, f].esEspecial)
                panelesSpike[c, f].DefEspecial();
            else
                panelesSpike[c, f].DefNormal();
        }
    }

    private void SetParedOfensivo(int c)
    {
        for (int i = 0; i < panelesSpike.GetLength(1); i++)
        {
            panelesSpike[c, i].Ofensivo();
        }
    }

    private void SetParedDefensivo(int c, bool esEspecial)
    {
        for (int i = 0; i < panelesSpike.GetLength(1); i++)
        {
            if(esEspecial)
                panelesSpike[c, i].DefNormal();
            else
                panelesSpike[c, i].DefEspecial();
        }
    }

    
    private void OnDestroy()
    {
        //Debug.Log("Escenario_Survival_Spike ELIMINADO AAAAHHHHHHHHHHHHHH!!!!!!!!!!!!!!11");
    }
}
