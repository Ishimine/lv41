﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moneda : MonoBehaviour {

    bool activado = true;
    public Collider2D col;
    public float tiempoDesaparicion = 2;
    IEnumerator rutinaPrimaria;

    public Vector3 lastMove;
    private float vel;

    public virtual void Start()
    {
        rutinaPrimaria = Aparecer();
        StartCoroutine(rutinaPrimaria);
    }
    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (!activado) return;
        if(other.tag == "Player")
        {
            GameController.instance.juego.esferaJugador.BurstParticulasGrande(transform.position, 7);

            //AdministradorPopText.instance.SetPos(transform.position);

            activado = false;
            StopAllCoroutines();
            rutinaPrimaria = Desaparecer(false);
            StartCoroutine(rutinaPrimaria);
        }        
    }

    IEnumerator Aparecer()
    {
        float dimInicial = 1;
        float dimMaxima = transform.localScale.x*1.2f;

        transform.localScale = Vector3.one;
        float x = 0;
        float a = 0;

        while (x < 1)
        {
            if (Time.timeScale == 0)
            {
                yield return null;
            }
            else
            {
                x += Time.deltaTime / tiempoDesaparicion;

                if (x < .5)
                {
                    a = Mathf.Lerp(0, dimMaxima, x * 2);
                    transform.localScale = new Vector3(a, a, a);
                }
                else
                {
                    a = Mathf.Lerp(dimMaxima, dimInicial / 2, x - .5f);
                    transform.localScale = new Vector3(a, a, a);
                }
            }
            yield return null;
        }
        transform.localScale = new Vector3(dimInicial, dimInicial, dimInicial);
        rutinaPrimaria = Flotando();
        StartCoroutine(rutinaPrimaria);
    }

    IEnumerator Flotando()
    {
        while (true)
        {
            Vector3 a  = Vector3.up * (Mathf.Sin(Time.realtimeSinceStartup * 7) / 25);
            transform.position = transform.position - lastMove + a;
            lastMove = a;
            yield return null;
        }
    }

   public IEnumerator Desaparecer(bool perdida)
    {
        if(!perdida) MonedaAtrapada();

        float dimInicial = transform.localScale.x;
        float dimMaxima = transform.localScale.x*1.45f;
        float x = 0;
        while(x < 1)
        {
            x += Time.deltaTime / tiempoDesaparicion;
            float a;
            if (x < .5)
            {
                a = Mathf.Lerp(dimInicial, dimMaxima, x*2);
                transform.localScale = new Vector3(a, a, a);
            }
            else
            {
                a = Mathf.Lerp(dimMaxima*2, 0, x);
                transform.localScale = new Vector3(a, a, a);
            }

            yield return null;
        }
        transform.localScale = new Vector3(dimInicial, dimInicial, dimInicial);
        if(perdida) MonedaPerdida();            
        Destroy(gameObject);
    }

    public void MonedaAtrapada()
    {
        ModoConMonedas modo = ChequearModoConMoneda();
        if (modo != null)
        {
            modo.MonedaAtrapada();
        }
    }

    public void MonedaPerdida()
    {
        ModoConMonedas modo = ChequearModoConMoneda();
        if (modo != null)
        {
            modo.MonedaPerdida();
        }
    }

    public ModoConMonedas ChequearModoConMoneda()
    {
        ModoConMonedas modo = GameController.instance.juego as ModoConMonedas;
        return modo;
    }


    public void OnDestroy()
    {
        Debug.Log(gameObject.name+" DESTRUIDAAAAA");
        StopAllCoroutines();

    }




}
