﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdaptarARatioPantalla : MonoBehaviour {


    public void Update()
    {
        Vector2 t = new Vector2(12, 22);
        Vector2 dimPantalla = new Vector2(Camera.main.orthographicSize / Screen.height * Screen.width * 2, Camera.main.orthographicSize * 2);
        transform.localScale = new Vector2(dimPantalla.x / t.x, dimPantalla.y / t.y);
        Destroy(this);
    }
}
