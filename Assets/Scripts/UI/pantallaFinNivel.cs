﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class pantallaFinNivel : MonoBehaviour {

    public ControlParticula p;


   [SerializeField]  DataDeNivel recordActual;
    [SerializeField] DataDeNivel objetivoActual;

    public Text tiempo;
    public Text muertes;
    public Text barras;
    public Text tiempoRec;
    public Text muertesRec;
    public Text barrasRec;

    public Image iconoMuerte;
    public Image iconoBarras;
    public Image iconoTiempo;

    int m;
    int b;
    float t;

    static pantallaFinNivel instance;
    public Contador contadorBarras;
    public Contador contadorMuertes;
    public Contador contadorTiempo;



    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
            SelectorNivel.NivelCargado += EnlazarEventos;
            gameObject.SetActive(false);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void EnlazarEventos()
    {
      //  GameController.PostFinSesionDeJuego += Activar;
    }

    public void Activar()
    {        
        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }
        ActualizarContadores();
    }

    public void ActualizarContadores()
    {
        t = GameController.GetTiempo();
        tiempo.text = "0.00";        

        b = TouchControlV2.BarrasCreadas;
        barras.text = "0";
        m = CheckpointManager.getMuertes();
        muertes.text = "0";


        ActualizarObjetivo();                           //Extrae los objetivos del nivel
        ExtraerRecord();                             //Actualiza los records del nivel
        ActivarContadores();
        //GuardarRecord();

        int x = SceneManager.GetActiveScene().buildIndex;
        AdministradorPuntaje.DesbloquearNivel(x);
        AdministradorPuntaje.DesbloquearNivel(++x);
        AdministradorPuntaje.DesbloquearNivel(++x);

        ActualizarEstadoDeNivel();
    }

    void ActualizarEstadoDeNivel()
    {
        if(recordActual.puntajes[0] <= objetivoActual.puntajes[0] &&           
           recordActual.puntajes[1] <= objetivoActual.puntajes[1] &&
           recordActual.puntajes[2] <= objetivoActual.puntajes[2])
        {
            AdministradorPuntaje.SetEstadoNivel(SceneManager.GetActiveScene().buildIndex-1, 1);
        }
    }


    /// <summary>
    /// Aplica records antiguos en los contadores y los activa las animaciones
    /// </summary>
    void ActivarContadores()
    {
        contadorBarras.Iniciar(b, objetivoActual.puntajes[0], recordActual.puntajes[0]);
        contadorMuertes.Iniciar(m, objetivoActual.puntajes[1], recordActual.puntajes[1]);
        contadorTiempo.Iniciar(t, objetivoActual.puntajes[2], recordActual.puntajes[2]);
    }

    IEnumerator RutinaComparar()
    {
        yield return new WaitForSeconds(0.1f);
        CompararObjetivos();
    }
 

    void CompararObjetivos()
    {
        if (ObjetivoCumplido(recordActual.puntajes[0], objetivoActual.puntajes[0]))
        {
            barras.text = recordActual.puntajes[0].ToString();
        }
        if (ObjetivoCumplido(recordActual.puntajes[2], objetivoActual.puntajes[2]))
        {
            tiempo.text = recordActual.puntajes[2].ToString("00:00.00");
        }
        if (ObjetivoCumplido(recordActual.puntajes[1], objetivoActual.puntajes[1]))
        {
            muertes.text = recordActual.puntajes[1].ToString();
        }
    }


    


    public void ActualizarObjetivo()
    {
        objetivoActual = AdministradorPuntaje.instance.getDataNivelObjetivos(SceneManager.GetActiveScene().buildIndex);
        
    }

    public void ExtraerRecord()
    {
        int nivelAct = SceneManager.GetActiveScene().buildIndex;
        if (nivelAct > 0)
        {
            recordActual = AdministradorPuntaje.instance.getDataNivelRecords(nivelAct);
        }
    }



    /*
    public void GuardarRecord()
    {
        int nivelAct = SceneManager.GetActiveScene().buildIndex;

        AdministradorPuntaje.ActualizarRecordNivel(nivelAct, new DataDeNivel(nivelAct, b, m, t));
    }*/


    public bool ObjetivoCumplido(int jugador, int objetivo)
    {
        return objetivo >= jugador;
    }

    public bool ObjetivoCumplido(float jugador, float objetivo)
    {
        return objetivo >= jugador;
    }   

    IEnumerator AnimarIcono(Text t,Image i)
    {
        yield return new WaitForSeconds(1f);
        float tiempo = 0.1f;
        while (i.color != Color.yellow)
        {
            t.color = Vector4.Lerp(t.color, Color.yellow, tiempo);
            i.color = t.color;
            yield return null;
        }
    }
}
