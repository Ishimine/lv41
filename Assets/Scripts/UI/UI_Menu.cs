﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Menu : UI_Base {

     void Awake()
    {
    }

    public void Home()
    {
        ActualizarEstado(0);
    }

    public void Iniciado()
    {
        ActualizarEstado(1);
    }

    public void MapaNiveles()
    {
        ActualizarEstado(1);
    }

    public void SelectorSurvival()
    {
        ActualizarEstado(4);
    }


}
