﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BotonPausa : MonoBehaviour {

    static BotonPausa instance;


    public Sprite pausaSprite;
    public Sprite playSprite;
    Image render;

    public AudioSource source;
    public AudioClip pausaIn;
    public AudioClip pausaOut;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            render = GetComponent<Image>(); CheckPausa();
            SelectorNivel.NivelCargado += CheckPausa;
            source = GetComponent<AudioSource>();
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void Pausa()
    {
        if (!GameController.enJuego)
            return;
        GameController.Pausa();
        CheckPausa();
    }

    void CheckPausa()
    {
        if (GameController.enPausa)
        {
            render.sprite = playSprite;
            source.clip = pausaIn;
            if (GameController.enJuego)
            {
                source.Play();
                MusicDJ.instance.GetComponent<AudioSource>().Pause();
            }
        }
        else
        {

            render.sprite = pausaSprite;
            source.clip = pausaOut;
            if (GameController.enJuego)
            {
                source.Play();
                MusicDJ.instance.GetComponent<AudioSource>().UnPause();
            }
        }

    }
}
