﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Survival : UI_GameMode {

    public float valor = 0;
    public Text txtContador;
    public Text txtContador_Izq;
    public Text txtContador_Der;


    public Text txtActual;
    public Text txtRecord;

    void Awake()
    { 
        SelectorNivel.NivelCargado += EnlazarEventos;
    }

    public void SetContadores(bool centro, bool izquierda, bool derecha)
    {
        txtContador.gameObject.SetActive(centro);
        txtContador_Izq.gameObject.SetActive(izquierda);
        txtContador_Der.gameObject.SetActive(derecha);
    }


    public void MostrarTabla()
    {

    }

    void EnlazarEventos()
    {
     //    GameController.AplicarConfiguracion += Inicializar;
         //TouchControlV2.instance.bCreada += actualizarContador;
    }

    public override void Desenlazar()
    {
      //  GameController.AplicarConfiguracion -= Inicializar;
    }

    public override void MostrarPuntaje(int actual, int record)
    {
        txtActual.text = actual.ToString();
        txtRecord.text = record.ToString();
    }
    

    public virtual void ActualizarIzq(string t)
    {
        txtContador_Izq.text = t;
    }

    public virtual void ActualizarDer(string t)
    {
        if(txtContador_Der != null)txtContador_Der.text = t;
    }
    

    public override void OnDestroy()
    {
        SelectorNivel.NivelCargado -= EnlazarEventos;

        base.OnDestroy();
    }

   
}
