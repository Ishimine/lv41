﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class UI_GameMode : UI_Base {

    public Text txt;

    public virtual void Pausa(bool x)
    {
        if (x)
            ActualizarEstado(0);
        else
            ActualizarEstado(1);
    }

    public abstract void MostrarPuntaje(int id, int value);

    public abstract void Desenlazar();

    public virtual void OnDestroy()
    {
        Desenlazar();
    }

    public virtual void ActualizarTxt(string t)
    {
        if(txt!= null) txt.text = t;  //HAY QUE CORREGIR ESTO SI SACAMOS EL IF OCURRE UN ERROR
    }


    public virtual void PreSesionDeJuego()
    {
        ActualizarEstado(3);
    }

    public virtual void FinSesionDeJuego()
    {
        ActualizarEstado(4);
    }



}
