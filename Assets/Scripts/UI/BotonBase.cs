﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public abstract class BotonBase : MonoBehaviour {

    public enum estados {Inhabilitado, Activado, Desactivado}
    public estados estado;

    public Image img;
    public Button boton;


    private void Awake()
    {
      if(img == null)  img = GetComponent<Image>();
      if(boton == null)  boton = GetComponent<Button>();



        AplicarEstado();
    }

    private void AplicarEstado()
    {
        Color c;
        switch (estado)
        {
            case estados.Inhabilitado:
                ColorUtility.TryParseHtmlString("#3F3F3F", out c);
                img.color = c;
                boton.interactable = false;
                break;
            case estados.Activado:
                boton.interactable = true;
                img.color = Color.white;
                break;
            case estados.Desactivado:
                boton.interactable = true;
                ColorUtility.TryParseHtmlString("#969696", out c);
                img.color = c;
                break;               
            default:
                break;
        }
    }


    public void CambiarEstado(estados est)
    {
        estado = est;
        AplicarEstado();
    }
    
    public abstract void BotonApretado();
}
