﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Base : MonoBehaviour {

    public Animator anim;


    /// <summary>
    /// 0 = Pausa
    /// 1 = InGame
    /// 3 = Home
    /// 4 = Fin De Juego
    /// </summary>
    /// <param name="x"></param>
    public virtual void ActualizarEstado(int x)
    {
        Debug.Log("Cambiando estado canvas a " + x);
        anim.SetInteger("Estado", x);
        anim.SetTrigger("Go");
    }




}
