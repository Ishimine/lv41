﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class canvasScript : MonoBehaviour {
    
    Animator anim;
    public GameObject inGameUI;
    public GameObject menuPrincipal;    
    public GameObject eventSys;
    public static canvasScript instance;

    public pantallaFinNivel pFinDeNivel;
    public UI_MenuNiveles menuNiveles;
    


    void Awake()
    {
        if (instance == null)
        {
            anim = GetComponent<Animator>();
            anim.SetInteger("Estado",5);
            instance = this;
        }      
        else
        {
            Destroy(eventSys);
            Destroy(this.gameObject);
        }
    }


    public void RevisarEstadoNiveles()
    {
        menuNiveles.RevisarEstadoNiveles();
    }

    public void Pausa(bool x)
    {
        if (x)
        {
            anim.SetInteger("Estado", 2);
        }
        anim.SetTrigger("Go");
    }


    public void ModoSurvival()
    {
        anim.SetInteger("Estado", 7);
        anim.SetTrigger("Go");
    }

    public void ModoNiveles()
    {
        inGameUI.SetActive(true);
        menuPrincipal.SetActive(false);
        anim.SetInteger("Estado", 1);
        anim.SetTrigger("Go");
    }


    public void Home()
    {
        anim.SetInteger("Estado", 4);
        anim.SetTrigger("Go");
        print("GameController.instance.Home()");
    }


    public void SeleccionDeNivel()
    {
        anim.SetInteger("Estado", 5);
        anim.SetTrigger("Go");
    }

    public void EnJuego()
    {
        anim.SetTrigger("Go");
    }


    public void CargandoEscena()
    {
        anim.SetInteger("Estado", 0);
            anim.SetTrigger("Go");
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            menuPrincipal.SetActive(true);
            inGameUI.SetActive(false);
        }
        else
        {
            inGameUI.SetActive(true);
            menuPrincipal.SetActive(false);
            reiniciarInGameUI();
        }
    }

 

    public void NivelTerminado()
    {
        anim.SetTrigger("Go");
        anim.SetInteger("Estado", 6);
    }
    



    void reiniciarInGameUI()
    {
        inGameUI.GetComponent<InGameUI>().Reiniciar();
    }

    


}
