﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillBox : MonoBehaviour
{
    public BoxCollider2D col;
    public float extra = 2;

    private void Start()
    {
        col.size  = new Vector2(Camera.main.orthographicSize / Screen.height * Screen.width *2 + extra, Camera.main.orthographicSize * 2 + extra);
    }



    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<EsferaJugador>().MuerteExplosiva();
        }
    }

}
