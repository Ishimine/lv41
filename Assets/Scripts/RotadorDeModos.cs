﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotadorDeModos : MonoBehaviour {

    public GameController.modo modoIzq;
    public GameController.modo modoDer;

    public Text txt;

    int modoActual = 1;

    public void Awake()
    {
        Debug.Log(gameObject.name + "Aaaaaaaaaaaaa");
    }
    public void Start()
    {
     
        AplicarModos();
    }


    public void Siguiente()
    {
        modoActual++;
        if (modoActual > ((int)AdministradorDeModos.instance.requisitosModos.Length/2))
        {
            modoActual = 1;
        }
        AplicarModos();
    }

    public void Anterior()
    {
        modoActual--;
        if (modoActual <= 0)
        {
            modoActual = ((int)AdministradorDeModos.instance.requisitosModos.Length/2);
        }
        AplicarModos();
    }

    public void AplicarModos()
    {        
        modoIzq = AdministradorDeModos.instance.requisitosModos[2 * (modoActual-1) + 1].modoReferencia;
        int indiceB = 2 * (modoActual - 1) + 2;
        if (indiceB < AdministradorDeModos.instance.requisitosModos.Length)
            modoDer = AdministradorDeModos.instance.requisitosModos[indiceB].modoReferencia;

        EscenarioMenu escMenu = FindObjectOfType<EscenarioMenu>();
        escMenu.AplicarModo(EscenarioMenu.Puerta.Izquierda, AdministradorDeModos.instance.requisitosModos[2 * (modoActual - 1) + 1]);

        if (indiceB < AdministradorDeModos.instance.requisitosModos.Length)
            escMenu.AplicarModo(EscenarioMenu.Puerta.Derecha, AdministradorDeModos.instance.requisitosModos[indiceB]);
        else
            escMenu.ModoVacio(EscenarioMenu.Puerta.Derecha);

        txt.text = modoActual.ToString();
    }



}
