﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MusicDJ : MonoBehaviour {

    public bool activado = false;
    public AudioClip[] pistas;
    public AudioSource fuente;
    public AudioLowPassFilter low;

    public static MusicDJ instance;

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
       // SelectorNivel.NivelCargado += Iniciarlizar;
        GameController.InicioSesionDeJuego += Iniciarlizar;
        Reproducir(0);
    }

    void EnlazarEventos()
    {
        GameController.AplicarConfiguracion += Iniciarlizar;
    }

    void OnDestroy()
    {
        GameController.AplicarConfiguracion -= Iniciarlizar;
    }

    public void LowPass(bool x)
    {
        low.enabled = x;
    }
    

    public void MedioVolumen()
    {
        fuente.volume = .5f;
    }

   

    public void Iniciarlizar()
    {
        if(low != null) LowPass(false);
        if (fuente == null) return;
        fuente.volume = 1;
        int i = SceneManager.GetActiveScene().buildIndex;

        
        if (i == 0)
        {
            Reproducir(i);
        }
           // Reproducir(Random.Range(1, pistas.Length) - 1);
        
        else if(i < 11)
        {
            Reproducir(1);
        }
        else if (i < 21)
        {
            Reproducir(2);
        }
        else
        {
            Reproducir(3);
        }
    }

    public void Reproducir(int id)
    {
        if (!activado) return;
        if (fuente.clip == pistas[id])
            return;
        fuente.clip = pistas[id];
        fuente.Play();
    }

}
