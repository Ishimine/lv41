﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoAlTrigger : MonoBehaviour {


    public AudioSource source;


    public void Desactivar()
    {
        Destroy(this);
    }
    private void Awake()
    {
        if (source == null)
        {
            source = GetComponent<AudioSource>();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            source.Play();
        }
    }
}
