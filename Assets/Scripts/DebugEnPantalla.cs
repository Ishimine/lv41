﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugEnPantalla : MonoBehaviour {

    public TextMesh txt;

    public static DebugEnPantalla instance;

    public void Awake()
    {
        if (instance == null) instance = this;
        else
            Destroy(this.gameObject);
        DontDestroyOnLoad(this.gameObject);
    }


    public void CambiarTexsto(string t)
    {
        txt.text = t;
    }
}
