﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class AdministradorDeModos : MonoBehaviour {


    public RequisitosDeModo[] requisitosModos;

    public static AdministradorDeModos instance;

    public GameMode_Menu modo_Menu;
    public GameMode_Survival_Classic modo_Classic;
    public GameMode_Survival_Restrained modo_Restrained;
    public GameMode_Survival_Spike modo_Spikes;
    public GameMode_Survival_TimeBomb modo_TimeBomb;


    public void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
        PrepararListaDeModo();  
    }
    
    public void PrepararListaDeModo()
    {
        requisitosModos = new RequisitosDeModo[6];
        requisitosModos[0] = modo_Menu.requisitos;
        requisitosModos[1] = modo_Classic.requisitos;
        requisitosModos[2] = modo_Restrained.requisitos;
        requisitosModos[3] = modo_Spikes.requisitos;
        requisitosModos[4] = modo_TimeBomb.requisitos;
        requisitosModos[5] = new RequisitosDeModo();
    }


    public static bool CargarRequisitos()
    {
        foreach(RequisitosDeModo r in instance.requisitosModos)
        {
           if(!instance.CargarDatos(ref r.desbloqueado, r.nombreOficial + "Desbloqueo"))
            return false;
        }
        return true;
    }

    public static void GuardarRequisitos()
    {
        foreach (RequisitosDeModo r in instance.requisitosModos)
        {
            instance.GuardarDatos(ref r.desbloqueado, r.nombreOficial + "Desbloqueo");
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="x"></param>
    /// <param name="valor"></param>
    public static void ActivarModo(GameController.modo x, float valor)
    {
       instance.requisitosModos[(int)x].DesbloquearModo(valor);
    }

    public static bool GetEstado(GameController.modo n)
    {
      return instance.requisitosModos[(int)n].desbloqueado;
    }

    public bool CargarDatos(ref bool ar, string nombreFisico)
    {
        if (File.Exists(Application.persistentDataPath + "/" + nombreFisico))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + nombreFisico, FileMode.Open);
            ar = (bool)bf.Deserialize(file);
            file.Close();
            return true;
        }
        else
            return false;
    }

    public void GuardarDatos(ref bool ar, string nombreFisico)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + nombreFisico);
        bf.Serialize(file, ar);
        file.Close();
    }
}
