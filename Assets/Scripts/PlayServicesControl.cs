﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;

public class PlayServicesControl : MonoBehaviour {
    

    public static PlayServicesControl instance;

    
	void Awake ()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        SignIn();
    }

    public void LogOut()
    {
        PlayGamesPlatform.Instance.SignOut();
    }

    static public void SignIn()
    {
        Social.localUser.Authenticate(success => {

            if(success)
            {
                Debug.Log("Login EXITOSO");
       // DebugEnPantalla.instance.CambiarTexto("Login EXITOSO");
            }
            else
            {
                Debug.Log("Login FALLIDO");
        //DebugEnPantalla.instance.CambiarTexto("Login FALLIDO");
            }
        });
    }


    #region Achivements
    public static  void UnlockAchivement(string id)
    {
        Social.ReportProgress(id, 100, success => {
            if (success)
            {
                Debug.Log("UnlockAchivement EXITOSO");
            }
            else
            {
                Debug.Log("UnlockAchivement FALLIDO");
            }
        });

    }



    static public void IncrementAchivement(string id, int stepsToIncrement)
    {
        PlayGamesPlatform.Instance.IncrementAchievement(id, stepsToIncrement, success => {
            if (success)
            {
                Debug.Log("IncrementAchivement EXITOSO");
            }
            else
            {
                Debug.Log("IncrementAchivement FALLIDO");
            }
        });

    }


    static public void ShowAchivementUI()
    {
       // DebugEnPantalla.instance.CambiarTexto("AchivementsUi");
        Social.ShowAchievementsUI();
    }

    #endregion


    #region LeaderBoard
    
    static public void ShowLeaderBoard(string id)
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(id);
    }
    
        static public void ShowAllLeaderBoards()
    {
        Social.ShowLeaderboardUI();        
    }

    static public void AddScoreToLeaderBoard(string id, long score)
    {
        Social.ReportScore(score, id, success => {
            if (success)
            {
                Debug.Log("AddScoreToLeaderBoard EXITOSO");
            }
            else
            {
                Debug.Log("AddScoreToLeaderBoard FALLIDO");
            }
        });

    }
    #endregion
    
    
}



