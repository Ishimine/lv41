﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonedaDeTiempo : Moneda {
    float angulo = 100;
    public int cantShakes = 3;
    public float tiempo = 8;
    public SpriteRenderer render;
    public SpriteRenderer borde;
    public Moneda_Shake shaker;

    public IEnumerator rutinaSecundaria;

    public override void Start()
    {
        base.Start();
        tiempo = CreadorDeMonedas.instance.tiempoPorMonedaAct;
        rutinaSecundaria = CuentaDeShake(tiempo);

        StartCoroutine(rutinaSecundaria);
    }
    

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.tag == "Player")
        {
            StopCoroutine(rutinaSecundaria);
        }
        borde.gameObject.SetActive(false);
        base.OnTriggerEnter2D(collision);        
    }

    IEnumerator CuentaDeShake(float x)
    {

        float paso = (1 / (float)(cantShakes+1));
        float ciclo = 1 - paso*2;


        float a = 1;

        do
        {
            a -= Time.deltaTime / x;
            if(a <= ciclo)
            {
                StartShake(a);
                ciclo -= paso;
            }
            yield return null;
        }
        while (a > 0);
        StartCoroutine(Desaparecer(true));
    }


    public void StartShake(float lerpValue)
    {
        Moneda_Shake.ShakeConfig c = Moneda_Shake.ShakeConfig.Lerp( shaker.shakeFuerte, shaker.shakeDebil, lerpValue);
        //c.duracion = tiempo / 3;
        shaker.IniciarShake(c);
    }
}
