﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraJugador : MonoBehaviour {

    public AudioSource source;
    public SpriteRenderer borde;
    public SpriteRenderer render;




    public IEnumerator rutina;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
        rutina = AnimacionBarraCuentaRegresiva();
    }

    public void OnEnable()
    {
        source.pitch = Random.Range(1.2f, 1.2f);
        source.Play();

        //Si se esta en modo survival se utiliza la animacion de desaparicion
        if (TouchControl.UsarBarraConTiempo && 
            GameController.instance.modoDeJuegoActivo > GameController.modo.Menu && 
            GameController.instance.modoDeJuegoActivo < GameController.modo.Niveles)
            StartCoroutine(rutina);
    }

   public IEnumerator AnimacionBarraCuentaRegresiva()
    {
        Color inicial = new Color(render.color.r, render.color.g, render.color.b, 0);
        Vector2 x = new Vector2(2f,5);
        float a = 0;
        float tiempo = TouchControl.instance.tVidaBarra;
        while (a < 1)
        {
            a += Time.deltaTime / tiempo;
            borde.color = Color.Lerp(inicial, render.color, a);
            borde.transform.localScale = Vector2.Lerp(x,Vector2.one, a);
            
            yield return null;
        }
        AdministradorPopText.instance.SetPos(transform.position);
        GameController.instance.juego.BarraPerdida();


        Destroy(gameObject);
        //Hacer que l barra muera luego de un tiempo y muestre una animacion, si muere bajo estas condiciones la barra resta puntos.
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
