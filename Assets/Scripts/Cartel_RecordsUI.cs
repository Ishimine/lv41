﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Cartel_RecordsUI : MonoBehaviour {

    public Animator anim;

    public Text txtBarras;
    public Text txtTiempo;
    public Text txtMuertes;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        SelectorNivel.NivelCargado += EnlazarEventos;
    }

    

    void EnlazarEventos()
    {
        if (GameController.instance.modoDeJuegoActivo == GameController.modo.Niveles)
        {
           // GameController.PreSesionDeJuego += Activar;             
        }
    }


    public void Activar()
    {
        anim.SetTrigger("Activar");
        ActualizarRecords();
    }

    void ActualizarRecords()
    {
        DataDeNivel records = AdministradorPuntaje.instance.getDataNivelObjetivos(SceneManager.GetActiveScene().buildIndex);
        txtBarras.text = records.puntajes[0].ToString();
        txtMuertes.text = records.puntajes[1].ToString();
        txtTiempo.text = records.puntajes[2].ToString("0:00.00");
    }

    private void OnDestroy()
    {
     //   GameController.PreSesionDeJuego -= Activar;
    }
}
