﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopText : MonoBehaviour {

    public TextMesh txt;
    float vel=1;
    public float altura = 3f;

    IEnumerator rutina;




    public void SetText(string t)
    {
        txt.text = t;
    }
    public void SetColor(Color c)
    {
        txt.color = c;
    }
    public void Activar()
    {
        rutina = Apagado();
        StartCoroutine(rutina);
    }

    public void Activar(Vector3 pos, string valor)
    {
        txt.text = valor;
        gameObject.transform.position = pos;
        rutina = Apagado();
        StartCoroutine(rutina);
    }



    IEnumerator Apagado()
    {
        float x = 0;
       // float altAct = transform.position.y;

        while (x < 1)
        {
            x += Time.deltaTime / vel;
            transform.Translate(Vector3.up * (altura * Time.deltaTime));
            yield return null;
        }
        gameObject.transform.position += new Vector3(0,0,-50);
    }
}
