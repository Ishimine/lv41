﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider2D))]
public class Lava : MonoBehaviour {

    IEnumerator contadorCheck;

    private void Awake()
    {
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        Rigidbody2D rb = col.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            rb.velocity = rb.velocity / 4;
            rb.gravityScale = 0.2f;
        }
        if (col.tag == "Player")
        {
            if (col.GetComponent<EsferaJugador>().dead == true) return;
            col.GetComponent<EsferaJugador>().dead = true;
            contadorCheck = ContadorCheck(col.gameObject);
            StartCoroutine(contadorCheck);
        }
        
    }



    IEnumerator ContadorCheck(GameObject player)
    {
        yield return new WaitForSeconds(.7f);
        StopCoroutine(contadorCheck);
        player.GetComponent<EsferaJugador>().Dead();
    }



    /*
    IEnumerator ContadorReinicioNivel()
    {
        Debug.Log("GameOver");
        yield return new WaitForSeconds(.7f);
        StopCoroutine("ContadorReinicioNivel");
        GameController.ReiniciarNivel();
    }
    */
    /*
    private void OnTriggerStay2D(Collider2D col)
    {
        Rigidbody2D rb = col.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            rb.gravityScale = 0.2f;
        }
    }*/

    private void OnTriggerExit2D(Collider2D col)
    {
        Rigidbody2D rb = col.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            rb.gravityScale = 1;
        }
    }
}
