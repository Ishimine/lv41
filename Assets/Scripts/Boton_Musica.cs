﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Boton_Musica : BotonBase {


    [SerializeField]   public AudioMixer musica;
    public bool prendido = true;


    private void Start()
    {
        CheckEstado();
    }

    public void CheckEstado()
    {
        if (prendido)
        {
            musica.SetFloat("VolMusica", -15);
        }
        else
        {
            musica.SetFloat("VolMusica", -80);
        }

        if (prendido)
        {
            CambiarEstado(estados.Activado);
        }
        else
        {
            CambiarEstado(estados.Desactivado);
        }
    }

    public override void BotonApretado()
    {
        if(prendido)
        {
            if(!musica.SetFloat("VolMusica", -80))
            {
                Debug.Log("Musica Apagada");
            }
        }
        else
        {
            musica.SetFloat("VolMusica", -15);
        }
        prendido = !prendido;

        if(prendido)
        {
            CambiarEstado(estados.Activado);
        }
        else
        {
            CambiarEstado(estados.Desactivado);
        }
    }
}
