﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_ComboMultiplicador : MonoBehaviour {

    public Text txtMult;
    public Text txtPaso;
    public Text txtPasoMax;
    public Image imagenCargaTiempoRestante;

 //   private float tMax;


    private void Awake()
    {
        txtPasoMax.text = AdministradorPuntaje.instance.combo.PasosNecesarios.ToString();
        txtPaso.text = AdministradorPuntaje.instance.combo.GetPasosActual().ToString();
        txtMult.text = AdministradorPuntaje.instance.combo.GetMultiplicadorActual().ToString("0");

        AdministradorPuntaje.instance.combo.actMult += ActualizarMultiplicador;
        AdministradorPuntaje.instance.combo.actPaso += ActualizarPaso;
        AdministradorPuntaje.instance.combo.actTiempo += ActualizarTiempoRestante;

        txtMult.transform.SetParent(transform.parent);
        txtMult.transform.SetSiblingIndex(2);
        //tMax = AdministradorPuntaje.instance.combo.TiempoMaxDeIngreso;


    }

    private void OnDestroy()
    {
        AdministradorPuntaje.instance.combo.actMult -= ActualizarMultiplicador;
        AdministradorPuntaje.instance.combo.actPaso -= ActualizarPaso;
        AdministradorPuntaje.instance.combo.actTiempo -= ActualizarTiempoRestante;
        Destroy(txtMult.gameObject);
    }

    public void ActualizarTiempoRestante(float act)
    {
        /*Debug.Log("Act:" + act);
        Debug.Log("Max:" + tMax);*/
        imagenCargaTiempoRestante.fillAmount = act/ AdministradorPuntaje.instance.combo.TiempoMaxDeIngreso;
    }

    public void ActualizarMultiplicador(float x)
    {
        if (x >= 1)
        {
            txtMult.gameObject.SetActive(true);
        }
        else
        {
            txtMult.gameObject.SetActive(false);
            txtMult.text = x.ToString("0.0");
        }
    }

    public void ActualizarPaso(int x)
    {
        txtPaso.text = x.ToString();
    }



}
