﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimadorDeFondoGrid2 : MonoBehaviour {

  public  Material fondoAnimadoGrid;

    public Renderer render;

    public void Awake()
    {
        fondoAnimadoGrid.EnableKeyword("_Tiempo");
    }

    public void Update()
    {
        //Debug.Log(Time.realtimeSinceStartup);
        render.material.SetFloat("_Tiempo", Time.realtimeSinceStartup/20);
    }
}
