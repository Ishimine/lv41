﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SelectorNivel : MonoBehaviour
{
   // public Slider slider;
   // public Text progresoTxt;
  //  public GameObject pantallaDeCarga;

    public static SelectorNivel instance;

    public delegate void trigger();

    public static event trigger NivelCargado;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //pantallaDeCarga.SetActive(false);
        }
        else
            Destroy(this.gameObject);            
    }

    void CambioDeEscena()
    {
       // Debug.Log("Escena cambiada");

        if (NivelCargado != null) NivelCargado();
    }

    public static void CargarNivel(int n)
    {
        if (n == SceneManager.GetActiveScene().buildIndex) return;

        Time.timeScale = 1;
        DirectorGravedad.ReestablecerGravedadInstantaneo();
        DirectorGravedad.LimpiarLista();
        
        if (n < SceneManager.sceneCountInBuildSettings)
        {
            instance.CargarAsincrono(n);
        }
        else
        {
            Debug.Log("Index de nivel inexistente");
            instance.CargarAsincrono(0);
        }        
    }

    public static void CargarModoSurvival()
    {
        instance.CargarAsincrono("Survival");
        //SceneManager.LoadScene("Survival");
       // Debug.Log("Cargando Survival");
    }


    public void CargarAsincrono(string n)
    {
        //pantallaDeCarga.SetActive(true);
        StartCoroutine(CargarNivelAsincrono(n));
    }

    public void CargarAsincrono(int n)
    {
        //pantallaDeCarga.SetActive(true);
        StartCoroutine(CargarNivelAsincrono(n));
    }

    public static void ReiniciarNivel()
    {
        //CargarNivel(SceneManager.GetActiveScene().buildIndex);
        GameController.ReiniciarNivelNormal();
    }


    public static void SiguienteNivel()
    {
        CargarNivel(SceneManager.GetActiveScene().buildIndex+1);
    }


    public static void NivelAnterior()
    {
        CargarNivel(SceneManager.GetActiveScene().buildIndex - 1);
    }


    public IEnumerator CargarNivelAsincrono(int i)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(i);       
        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);            
            yield return null;
        }
        CambioDeEscena();
    }

    public IEnumerator CargarNivelAsincrono(string i)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(i);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);            
            yield return null;
        }
        CambioDeEscena();

    }
}
