﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelSpike_Defensivo : MonoBehaviour {

    public PanelSpike panel;



    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            ChequearModoConPanelesMoneda();     //Genera Puntaje

            panel.Tocado();             //Alerta al escenario para cambiar???
        }
    }
    

    public void ChequearModoConPanelesMoneda()
    {
        ModoConPanelesMonedas modo = GameController.instance.juego as ModoConPanelesMonedas;
        if(modo != null)
        {
            if (panel.esEspecial)
                modo.PanelMonedaEspecialAtrapado();
            else
                modo.PanelMonedaAtrapado();
        }
    }

}
