﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wrap : MonoBehaviour {


    public bool activadoEnX = true;
    public bool activadoEnY = false;
    public Vector2 dimensionPantalla;
    float distExtra = .5f;


    //GameObject[] copias;

    void Awake()
    {
        dimensionPantalla = new Vector2(Camera.main.orthographicSize / Screen.height * Screen.width + distExtra, Camera.main.orthographicSize);
    }   

    void Update()
    {
        if (activadoEnX && SalePorX())
        {
            DesplazarX(dimensionPantalla.x);
        }

        if (activadoEnY && SalePorY())
        {
            DesplazarY(dimensionPantalla.y);
        }
    }

    public bool SalePorX()
    {
        if ((gameObject.transform.position.x < (dimensionPantalla.x + distExtra/2)) && (gameObject.transform.position.x > (-dimensionPantalla.x - distExtra/2)))
            return false;
        else
            return true;
    }

    public bool SalePorY()
    {
        if ((gameObject.transform.position.y < dimensionPantalla.y + distExtra/2) && (gameObject.transform.position.y > -dimensionPantalla.y - distExtra/2))
            return false;
        else
            return true;
    }

    void DesplazarX(float x)
    {
        transform.position += new Vector3((x * 2  * (-Mathf.Sign(transform.position.x))) + distExtra/2, 0);
    }

    void DesplazarY(float x)
    {
        transform.position += new Vector3(0,(x * 2 * (-Mathf.Sign(transform.position.y)) ) + distExtra/2) ;
    }



    public void Desplazar(Vector2 dir)
    {
        transform.position += new Vector3(dimensionPantalla.x * Mathf.Sign(dir.x), dimensionPantalla.y * Mathf.Sign(dir.y));
    }
}
