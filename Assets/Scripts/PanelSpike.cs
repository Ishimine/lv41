﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PanelSpike : MonoBehaviour
{
    public Escenario_Survival_Spike esc;

    public SpriteRenderer render;
    public SpriteRenderer borde;
    public float tiempo = 5f;
    bool usarTiempo = false;
    public bool esEspecial = false;

    public SpriteRenderer renderDefensivo;
    public SpriteRenderer renderOfensivo;

    public Color colorDefNormal;
    public Color colorDefEspecial;
    public Color colorfensivo;

    IEnumerator rutina;

    public void Ofensivo()
    {
        if (rutina != null) StopCoroutine(rutina);
        borde.color = Color.clear;  

        renderDefensivo.gameObject.SetActive(false);
        renderOfensivo.gameObject.SetActive(true);
    }

    public void DefNormal()
    {
        Def();
        renderDefensivo.color = colorDefNormal;
        if (usarTiempo)
        {
            rutina = DesaparicionBarra();
            StartCoroutine(rutina);
        }
    }

    public void DefEspecial()
    {
        Def();
        renderDefensivo.color = colorDefEspecial;
        if(usarTiempo)
        {
            rutina = DesaparicionBarra();
            StartCoroutine(rutina);
        }
    }

    private void OnDestroy()
    {
       if(rutina != null)  StopCoroutine(rutina);
    }

    private void Def()
    {
        renderDefensivo.gameObject.SetActive(true);
        renderOfensivo.gameObject.SetActive(false);
    }

    public void SetEscenario(Escenario_Survival_Spike x)
    {
        esc = x;
    }

    
    public void Tocado()
    {
        esc.PanelTocado();
    }

    IEnumerator DesaparicionBarra()
    {
        Color inicial = new Color(render.color.r, render.color.g, render.color.b, 0);
        Vector2 dim = new Vector2(4, 2);
        float a = 0;
        while (a < 1)
        {
            a += Time.deltaTime / tiempo;
            borde.color = Color.Lerp(inicial, render.color, a);
            borde.transform.localScale = Vector2.Lerp(dim, Vector2.one, a);
            yield return null;
        }
        if (GameController.instance.juego != null) GameController.instance.juego.FinSesionDeJuego();
        Destroy(gameObject);
    }

}
