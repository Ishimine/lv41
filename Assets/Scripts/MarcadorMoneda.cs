﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarcadorMoneda : MonoBehaviour {

    public SpriteRenderer render;

    IEnumerator rutina;



    public void Activar(float t)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Animacion(t);
        StartCoroutine(rutina);
    }

    public void Desactivar()
    {
        if (rutina != null) StopCoroutine(rutina);
        gameObject.SetActive(false);
    }

    public void OnDestroy()
    {
      if(rutina!= null)  StopCoroutine(rutina);
    }


    IEnumerator Animacion(float t)
    {
        //render.transform.localScale = Vector3.zero;
        //render.color = Color.clear;
        float x = 0; 
        while(x < 1)
        {
            x += Time.deltaTime / t;
            float a = Mathf.Lerp(0, 1f,x);
            //render.transform.localScale = new Vector3(a,a,a);x

            render.color = new Color(render.color.r, render.color.g, render.color.b, a);
            yield return null;
        }
    }
}
