﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Enum = System.Enum;
using UnityEngine.SceneManagement;

public class AdministradorPuntaje : MonoBehaviour
{

    private int idNivel_01;
    public static AdministradorPuntaje instance;

    /// <summary>
    /// [0] Monedas, [1]Interruptores , [2]Otros
    /// </summary>
    public int[] valorPremio = new int[3];
    public int[] valorPenalizacion = new int[3];




    /// <summary>
    /// puntajeActual[0] = puntaje numerico estandar, puntajeActual[1] = muertes, puntajeActual[2] = tiempo
    /// </summary>
    public DataDeNivel puntajeActual;                  //Puntaje actual del jugador en el nivel en juego


    public DataDeNivel recordActual;                   //Record del nivel actual
    public DataDeNivel objetivoActual;                 //Objetivo del nivel actual


    public DataDeNivel[] recordsAux;            //Arreglo de records de los niveles standares
    public DataDeNivel[] objetivosAux;          //Arreglo de records de los objetivos standares


    public delegate void acttualizacionPuntaje(int idPuntaje, float valor);
    public acttualizacionPuntaje actPuntaje;


    private bool usarPopText = false;
    public bool UsarPopText
    {
        get
        {
            return usarPopText;
        }
        set
        {
            usarPopText = true;
        }
    }

    private bool usarComboMultiplicador = false;
    public bool UsarComboMultiplicador
    {
        get
        {
            return usarComboMultiplicador;
        }
        set
        {
            usarComboMultiplicador = value;
        }
    }


    public ComboMultiplicador combo;

    


    public void Awake()
    {
        //Debug.Log("CARAJO");
        if (instance == null)
        {           
            instance = this;
            puntajeActual = new DataDeNivel(1, 0, 0, 0);
            BuscarIdNivel_01();
            GameController.AplicarConfiguracion += ResetearPuntaje;
            GameController.AplicarConfiguracion += CargarDatosDeNivel;


            recordActual = new DataDeNivel(-1, 0, 0, 0);
            objetivoActual = new DataDeNivel(-1,0,0,0);
            combo = new ComboMultiplicador();
            combo.EnlazarSistema();
            GameController.InicioSesionDeJuego += combo.ResetearValores;
           // combo.ResetearValores;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDestroy()
    {
        GameController.AplicarConfiguracion -= ResetearPuntaje;
        GameController.AplicarConfiguracion -= CargarDatosDeNivel;
    }



    public void ConfigurarComboMultiplicador(bool usarCombo, int pasosNecesarios, float intensidadMultiplicador, float multMax, float tiempoMaxDeIngreso)
    {

        UsarComboMultiplicador = usarCombo;
        combo.PasosNecesarios = pasosNecesarios;
        combo.MultiplicadorMaximo = multMax;
        combo.MultiplicadorMaximo = multMax;
        combo.TiempoMaxDeIngreso = tiempoMaxDeIngreso;
        combo.Iniciar();

    }

    void ResetearPuntaje()
    {

        valorPremio[0] = 10;
        valorPremio[1] = 20;
        valorPremio[2] = 30;
        valorPenalizacion[0] = 10;
        valorPenalizacion[1] = 20;
        valorPenalizacion[2] = 30;
        puntajeActual.idNivel = 1;
        puntajeActual.puntajes[0] = 0;
        puntajeActual.puntajes[1] = 0;
        puntajeActual.puntajes[2] = 0;
    }

    public static void ComboBreaker()
    {
        instance.combo.ComboBreaker();
    }
    public void SetValorPremio(int id, int valor)
    {
        valorPremio[id] = valor;
    }

    public void SetValorPenalizacion(int id, int valor)
    {
        valorPenalizacion[id] = valor;
    }

    ///Busca el ID del "Nivel_1" (Se debe respetar el nombre). Este id se usa como base para calcular donde inicia el arreglo de niveles.
    private void BuscarIdNivel_01()
    {
        idNivel_01 = SceneManager.GetSceneByName("Nivel_1").buildIndex;
    }

    ///Carga Record y Objetivos del Nivel/Modo actual
    public void CargarDatosDeNivel()
    {
        if(GameController.instance.modoDeJuegoActivo == GameController.modo.Menu)
        {
            CargarDatosMenu();
        }
        else if (GameController.instance.modoDeJuegoActivo == GameController.modo.Niveles)
        {
            CargarDatosNivel();
        }
        else if (GameController.instance.modoDeJuegoActivo > GameController.modo.Menu && GameController.instance.modoDeJuegoActivo < GameController.modo.Niveles)
        {
            CargarDatosSurvival();

        }        
    }

    private void CargarDatosMenu()
    {
    }

    private void CargarDatosSurvival()
    {
        CargarDatos(ref recordActual, "Survival_Record_" + GameController.instance.modoDeJuegoActivo.ToString());
    }

    private void GuardarDatosSurvival()
    {
        puntajeActual = SobreescribirPuntaje(puntajeActual, recordActual);
        GuardarDatos(ref puntajeActual, "Survival_Record_" + GameController.instance.modoDeJuegoActivo.ToString());
        
        switch (GameController.instance.modoDeJuegoActivo)
        {
            case GameController.modo.Survival_Classic:
                PlayServicesControl.AddScoreToLeaderBoard(GPGSIds.leaderboard_classic, (long)puntajeActual.puntajes[0]);
                break;
            case GameController.modo.Survival_Restrained:
                PlayServicesControl.AddScoreToLeaderBoard(GPGSIds.leaderboard_limited, (long)puntajeActual.puntajes[0]);
                break;
            case GameController.modo.Survival_Spike:
                PlayServicesControl.AddScoreToLeaderBoard(GPGSIds.leaderboard_spikes, (long)puntajeActual.puntajes[0]);
                break;
            case GameController.modo.Survival_TimeBomb:
                PlayServicesControl.AddScoreToLeaderBoard(GPGSIds.leaderboard_time_bomb, (long)puntajeActual.puntajes[0]);
                break;
            case GameController.modo.Survival_Rain:
                break;
            case GameController.modo.Survival_MoneyHunter:
                break;
            case GameController.modo.Survival_Twister:
                break;
            default:
                break;
        }
        
    }

    private void CargarDatosNivel()
    {
        CargarRecordsNiveles();
        CargarObjetivosNiveles();
        objetivoActual = ExtraerObjetivoNivel();
        recordActual = ExtraerRecordNivel();
    }

    public void CargarRecordsNiveles()
    {
        CargarDatos(ref recordsAux, "Niveles_Records");
    }

    public void CargarObjetivosNiveles()
    {
        CargarDatos(ref objetivosAux, "Niveles_Objetivos");
    }

 


    /// <summary>
    /// [0]=Moneda Perdida [1]=Barra Perdida [2]=Otros
    /// </summary>
    /// <param name="idPuntaje"></param>
    /// <param name="idPenalizacion"></param>
    public void RestaPenalizacionPuntaje(int idPuntaje, int idPenalizacion)
    {
        Debug.Log("Penalizacion ID:" + idPenalizacion + " = " + valorPenalizacion[idPenalizacion]);
        puntajeActual.puntajes[idPuntaje] -= valorPenalizacion[idPenalizacion];
        if(puntajeActual.puntajes[idPuntaje] < 0) puntajeActual.puntajes[idPuntaje] =0;
        if (actPuntaje != null) actPuntaje(idPuntaje, puntajeActual.puntajes[idPuntaje]);
    }




    public float SumarPuntaje(int id, float valor)
    {
        if(usarComboMultiplicador && valor > 0) valor = combo.IngresoDePuntaje(valor);
        puntajeActual.puntajes[id] += valor;
        if (puntajeActual.puntajes[id] < 0) puntajeActual.puntajes[id] = 0;
        if (actPuntaje != null) actPuntaje(id, puntajeActual.puntajes[id]);

        if (usarPopText)
        {
            string signo = "";
            if (valor < 0)
            {
                AdministradorPopText.instance.SetColor(Color.red);
                signo = "-";
            }
            else
            {
                AdministradorPopText.instance.SetColor(Color.yellow);
            }

            AdministradorPopText.instance.SetTexto(signo + valor.ToString("0"));
            AdministradorPopText.instance.Activar();
        }
        return valor;
    }
    

    ///Guarda Valor de puntaje actual por id
    public void SetPuntaje(int id, float valor)
    {
        //Debug.Log("SetPuntaje: " + Time.time);
        puntajeActual.puntajes[id] = valor;
        if (actPuntaje != null) actPuntaje(id, puntajeActual.puntajes[id]);
    }

    public void SetPuntaje(DataDeNivel d)
    {
        puntajeActual = d;
        if (actPuntaje != null) actPuntaje(0, d.puntajes[0]);
        if (actPuntaje != null) actPuntaje(1, d.puntajes[1]);
        if (actPuntaje != null) actPuntaje(2, d.puntajes[2]);
    }

  
    ///Devuelva valor de puntaje actual definido por id
    public float GetPuntaje(int id)
    {
        return puntajeActual.puntajes[id];
    }

    ///Devuelve el objeto DataDeNivel
    public DataDeNivel GetPuntaje()
    {
        return puntajeActual;
    }

    public DataDeNivel GetRecord()
    {
        return recordActual;
    }
    public DataDeNivel GetObjetivo()
    {
        return objetivoActual;
    }

    /// <summary>
    /// Guarda puntaje actual del Modo/Nivel
    /// </summary>
    public void GuardarPuntajeActual()
    {
       // Debug.Log("GuardandoPuntaje: " + GameController.instance.modoDeJuegoActivo);

        if (GameController.instance.modoDeJuegoActivo == GameController.modo.Menu)
        {
        }
        else if (GameController.instance.modoDeJuegoActivo == GameController.modo.Niveles)
        {
            GuardarPuntajeNivel();
            GuardarDatosNivel();
        }
        else if (GameController.instance.modoDeJuegoActivo > GameController.modo.Menu && GameController.instance.modoDeJuegoActivo < GameController.modo.Niveles)
        {
            GuardarDatosSurvival();

            switch (GameController.instance.modoDeJuegoActivo)      //Para desbloquear los otros modos
            {
                case GameController.modo.Menu:
                    break;
                case GameController.modo.Survival_Spike:
                    break;
                case GameController.modo.Survival_Rain:
                    break;
                case GameController.modo.Survival_MoneyHunter:
                    break;
                case GameController.modo.Survival_Twister:
                    break;
                case GameController.modo.Survival_TimeBomb:
                    break;
                case GameController.modo.Niveles:
                    break;
                default:
                    break;
            }
            switch (GameController.instance.modoDeJuegoActivo)
            {
                case GameController.modo.Menu:
                    break;
                case GameController.modo.Survival_Classic:
                    AdministradorDeModos.ActivarModo(GameController.modo.Survival_Spike, puntajeActual.puntajes[0]);
                    break;
                case GameController.modo.Survival_Restrained:
                    AdministradorDeModos.ActivarModo(GameController.modo.Survival_TimeBomb, puntajeActual.puntajes[0]);
                    break;
                case GameController.modo.Survival_Spike:
                    AdministradorDeModos.ActivarModo(GameController.modo.Survival_Restrained, puntajeActual.puntajes[0]);
                    break;
                case GameController.modo.Survival_TimeBomb:
                    break;
                case GameController.modo.Survival_Rain:
                    break;
                case GameController.modo.Survival_MoneyHunter:
                    break;
                case GameController.modo.Survival_Twister:
                    break;
                case GameController.modo.Niveles:
                    break;
                default:
                    break;
            }




            AdministradorDeModos.GuardarRequisitos();

        }
    }
    
               
    /// <summary>
    /// Compara Puntaje actual con el recordActual y sobreescribe los valores mas altos;
    /// </summary>
    void GuardarPuntajeNivel()
    {
        DataDeNivel x = SobreescribirPuntaje(puntajeActual, ExtraerRecordNivel());
        AlmacenarObjetivoDeNivel(x);
    }


    /// <summary>
    /// Toma 2 set de datos de nivelse y devuelve un nuevo DatoDeNivel con los valores mas altos
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    DataDeNivel SobreescribirPuntaje(DataDeNivel a, DataDeNivel b)
    {
        //Debug.Log("Sobreescribiendo Puntajes: " + a.puntajes[0] + "  " + b.puntajes[0]);
        a.puntajes[0] = Mathf.Max(a.puntajes[0], b.puntajes[0]);
        a.puntajes[1] = Mathf.Max(a.puntajes[1], b.puntajes[1]);
        a.puntajes[2] = Mathf.Max(a.puntajes[2], b.puntajes[2]);
        return a;
    }

    /// <summary>
    /// Guarda los arreglos de Records y de Objetivos aux en el disco
    /// </summary>
    void GuardarDatosNivel()
    {
        GuardarRecordsNiveles();
        GuardarObjetivosNiveles();
    }
    public void GuardarRecordsNiveles()
    {
        GuardarDatos(ref recordsAux, "Niveles_Records");
    }
    public void GuardarObjetivosNiveles()
    {
        GuardarDatos(ref objetivosAux, "Niveles_Objetivos");
    }



    /// <summary>
    /// Almacena Objetivo actual en el arreglo de objetivos aux
    /// </summary>
    /// <param name="d"></param>
    void AlmacenarObjetivoDeNivel(DataDeNivel d)
    {
        objetivosAux[SceneManager.GetActiveScene().buildIndex - idNivel_01] = d;
    }
    /// <summary>
    /// Almacena el record actual en el arreglo de records aux
    /// </summary>
    /// <param name="d"></param>
    void AlmacenarRecordDeNivel(DataDeNivel d)
    {
        recordsAux[SceneManager.GetActiveScene().buildIndex - idNivel_01] = d; 
    }


    DataDeNivel ExtraerRecordNivel(int idNivel)
    {
        return recordsAux[idNivel];
    }
    DataDeNivel ExtraerRecordNivel()
    {
       return ExtraerRecordNivel(SceneManager.GetActiveScene().buildIndex - idNivel_01);
    }
    DataDeNivel ExtraerObjetivoNivel(int idNivel)
    {
        return objetivosAux[idNivel];
    }
    DataDeNivel ExtraerObjetivoNivel()
    {
        return ExtraerObjetivoNivel(SceneManager.GetActiveScene().buildIndex - idNivel_01);
    }



    #region Guardado y Carga de datos en disco

    public static void CargarDatos(ref DataDeNivel[] ar, string nombreFisico)
    {
        if (File.Exists(Application.persistentDataPath + "/" + nombreFisico))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + nombreFisico, FileMode.Open);
            ar = (DataDeNivel[])bf.Deserialize(file);
            file.Close();
        }
    }
    public static void CargarDatos(ref DataDeNivel ar, string nombreFisico)
    {
        if (File.Exists(Application.persistentDataPath + "/" + nombreFisico))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + nombreFisico, FileMode.Open);
            ar = (DataDeNivel)bf.Deserialize(file);
            file.Close();
        }
    }
    public static void GuardarDatos(ref DataDeNivel[] ar, string nombreFisico)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + nombreFisico);
        bf.Serialize(file, ar);
        file.Close();
    }
    public static void GuardarDatos(ref DataDeNivel ar, string nombreFisico)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + nombreFisico);
        bf.Serialize(file, ar);
        file.Close();
    }
    #endregion


    public void DesbloquearNiveles()
    {
        for (int i = idNivel_01; i < recordsAux.Length + idNivel_01; i++)
        {
            recordsAux[i].idNivel = 0;
        }
        GuardarRecordsNiveles();
    }


   


    public DataDeNivel getDataNivelObjetivos(int i)
    {
        return objetivosAux[i-idNivel_01];
    }        

    public DataDeNivel getDataNivelRecords(int i)
    { 
        return recordsAux[i-idNivel_01];
    }



    public static int GetEstadoNivel(int x)
    {
       return instance.recordsAux[x].idNivel;
    }

    public static void DesbloquearNivel(int x)
    {
        if (instance.recordsAux[x].idNivel == 1) return;
        else        instance.recordsAux[x].idNivel = 0;
    }

    public static void SetEstadoNivel(int id, int estado)
    {
        instance.recordsAux[id].idNivel = estado;
        instance.GuardarRecordsNiveles();
    }

    public void ResetRecords()
    {
        ResetearRecordsSurvival();
        for (int i = 0; i < recordsAux.Length; i++ )
        {
            recordsAux[i] = new DataDeNivel();
        }
        recordsAux[0].idNivel = 0;
        GuardarRecordsNiveles();
    }

    public void ResetearRecordsSurvival()
    {
        puntajeActual = new DataDeNivel(-1, 0, 0, 0);
        foreach (string m in Enum.GetNames(typeof(GameController.modo)))
        {
            if (m == "Niveles" || m == "Menu") continue;
            GuardarDatos(ref puntajeActual, "Survival_Record_" + m);
        }
    }
}
