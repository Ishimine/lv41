﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ModoConMonedas {

    void InstanciarCreadorDeMonedas();
    CreadorDeMonedas GetCreadorDeMonedas();

    void MonedaAtrapada();
    void MonedaPerdida();
    void MonedaEspecialAtrapada();
    void MonedaEspecialPerdida();



}
