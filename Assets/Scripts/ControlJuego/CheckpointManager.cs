﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckpointManager : MonoBehaviour {

    static int muertes = 0;

    public static CheckpointManager instance;
    public CheckPoint[] puntos;
    public static int CheckPointActual = -1;
    public static EsferaJugador player;
    Vector3 inicial;

    public delegate void act(int i);
    public static act muerte;

    public static int getMuertes()
    {
        return muertes;
    }

    private void Awake()
    {
        if (instance == null) instance = this;
        else { Destroy(this.gameObject); }
        GameController.InicioSesionDeJuego += Inicializar;
    }

    public static void Reiniciar()
    {
        foreach(CheckPoint cp in instance.puntos)
        {
            cp.PuntoDesactivado();
        }
        CheckPointActual = -1;
        muertes = 0;
        if (muerte != null) muerte(0);
        instance.CheckPointNormal(instance.inicial);

    }

    void Inicializar()
    {
        inicial = GameObject.FindGameObjectWithTag("pPartida").transform.position;
        muertes = 0;
        CheckPointActual = -1;
        player = FindObjectOfType<EsferaJugador>();
        //player.muerto += LastCheckPoint;
        puntos = FindObjectsOfType<CheckPoint>();

    }

    public static void UsarUltimoCP()
    {
        instance.LastCheckPoint();
    }

        public void LastCheckPoint()
    {

        player.trail.SetActive(false);
        muertes++;
        if (muerte != null) muerte(muertes);
        Vector3 target;

        if (CheckPointActual != -1)
        {
            target = puntos[CheckPointActual].transform.position;
            CheckPointNormal(target);
        }
        else
        {
            print("Reiniciar nivel");
           //GameController.ReiniciarNivel();
        }
    }

    public void CheckPointOriginal(Vector3 target)
    {
        //GameController.ReiniciarContadores();
            print("Reiniciar Contadores");
        CheckPointNormal(target);
    }

    public void CheckPointNormal(Vector3 target)
    {
        TouchControlV2.instance.DeshabilitarBarras();
        player.transform.position = target;
        //Camera.main.GetComponent<SeguirObjetivo>().PosicionarCamara(target);
        player.GetComponent<Rigidbody2D>().velocity = Physics2D.gravity.normalized * -1;
        player.GetComponent<Rigidbody2D>().angularVelocity = 0;
        DirectorGravedad.ReestablecerGravedadInstantaneo();
        //player.activarTrail();
        player.trail.SetActive(true);
        player.dead = false;
        player.RevivirJugador();
        Camera.main.transform.position = new Vector3(target.x, target.y, -10);
       // Camera.main.GetComponent<CameraFollow>().SetCenter(target);
        
        TouchControlV2.instance.DeshabilitarBarras();
    }

    public static void ActualizarPunto(int i)
    {
        CheckPointActual = i;
        Debug.Log("CP Actualizado a " + i);
        
    }


}
