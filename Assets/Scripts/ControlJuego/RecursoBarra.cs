﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class RecursoBarra
{

    public delegate void act(float x);

    public act actSumar;
    public act actPrevisualizar;
    public act actRestar;



    private float maximo;

    public float Maximo
    {
        get { return maximo; }
        set { maximo = value; }
    }

    private float actual;

    public float Actual
    {
        get { return actual; }
        set
        {
            actual = value;
            if(actual > maximo)  actual = maximo;            
        }
    }

    public bool PuedeConsumir(float x)
    {
        if (x <= actual)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool Consumir(float x)
    {
        if (x <= actual)
        {
            actual -= x;
            if (actRestar != null) actRestar(actual);
            /*Debug.Log("Consumido: " + x);
            Debug.Log("Maximo: " + maximo);
            Debug.Log("Actual" + actual);*/
            return true;
        }
        else
        {
            return false;
        }

    }


    /// <summary>
    /// Devuelve la cantidad de recurso disponible en relacion a la cantidad solicitada
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public float SolicitarDisponibilidadRecurso(float x)
    {
        float aux;
        if(x < actual)        
            aux = x;        
        else        
            aux = actual;        
        if (actPrevisualizar != null) actPrevisualizar(actual - aux);
        return aux;

    }

    public void SetMax(float m)
    {
        Maximo = m;
        Actual = m;
    }


    public void Recargar(float x)
    {
        Actual += x;
        if (actSumar != null) actSumar(Actual);
    }


    public RecursoBarra(float max)
    {
        Maximo = max;
        Actual = max;
    }
}
