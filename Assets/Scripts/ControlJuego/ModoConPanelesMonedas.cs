﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ModoConPanelesMonedas
{

    
    void PanelMonedaAtrapado();
    void PanelMonedaPerdido();

    void PanelMonedaEspecialAtrapado();
    void PanelMonedaEspecialPerdido();


}
