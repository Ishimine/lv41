﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;


public class GameController : MonoBehaviour
{
    public enum modo { Menu, Survival_Classic, Survival_Restrained, Survival_Spike, Survival_TimeBomb, Survival_Rain, Survival_MoneyHunter, Survival_Twister, Niveles }
    public modo modoDeJuegoActivo;

    static public GameController instance;
    float tiempoDeJuego;




    public GameMode juego;
    public GameObject canvasActual;

    public GameObject player;
    public GameObject meta;
    public Transform pPartida;
    public Transform pMeta;

    public static bool enPausa = false;
    public static bool enJuego = false;
    public static bool swipeActivo = true;


    public delegate void actFloat(float tiempo);
    public event actFloat actTiempo;
    public delegate void Gatillo();
    public static event Gatillo InstanciacionGeneral;
    public static event Gatillo AplicarConfiguracion;
    public static event Gatillo InicioSesionDeJuego;
    public static event Gatillo FinSesionDeJuego;
    public static event Gatillo PausaIn;
    public static event Gatillo PausaOut;
    public static event Gatillo CierreDeEscena;

    

    public delegate void UpdateEvent(float x);
    public static event UpdateEvent gcUpdate;
    public static event UpdateEvent gcFixedUpdate;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instance = this;
            actTiempo = null;
           // pausado = null;
            this.gameObject.SetActive(true);
            CargarModoDeJuego();
            SelectorNivel.NivelCargado += NivelCargado;

            if (SceneManager.GetActiveScene().buildIndex == 0)  modoDeJuegoActivo = modo.Menu;
          
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    private void Start()
    {
        //      Debug.Log("SSTART GAME CONTROLLER");
        //      CambiarModoDeJuego(modo.Menu);
        //     CargarModoDeJuego();
        //    Juego_InstanciacionGeneral();
        Juego_InstanciacionGeneral();
        Juego_AplicarConfiguracion();
    }

    void Juego_InstanciacionGeneral()
    {
        if (InstanciacionGeneral != null) InstanciacionGeneral();
    }

    public void OnApplicationPause(bool pause)
    {
        //Pausa();
    }

    

    private void Update()
    {
        if (gcUpdate != null) gcUpdate(Time.deltaTime);
        if (enJuego && !enPausa)
        {
            tiempoDeJuego += Time.deltaTime;
            if (actTiempo != null) actTiempo(tiempoDeJuego);
        }

        if(Input.GetKeyDown(KeyCode.Escape) && SceneManager.GetActiveScene().buildIndex != 0)   //Test boton back
        {
            CambiarModoDeJuego(modo.Menu);
            CargarModoDeJuego();
        }
    }

    private void FixedUpdate()
    {
        if (gcFixedUpdate != null) gcFixedUpdate(Time.fixedDeltaTime);
    }

    public static void Pausa()
    {
        if (!enPausa)
        {
            //canvasScript.instance.Pausa(true);
            swipeActivo = false;
            Time.timeScale = 0;
        }
        else
        {
            //canvasScript.instance.Pausa(false);
            swipeActivo = true;
            Time.timeScale = 1;
        }
        enPausa = !enPausa;
   
    }



    public static void ReiniciarNivelNormal()
    {
        Juego_AplicarConfiguracion();
    }


    /// <summary>
    /// Aplica configuracion del modo y luego la PRE sesion de juego
    /// </summary>
    private void NivelCargado()
    {
        Debug.Log("Nivel Cargado");
        CargarModoDeJuego();
        Juego_InstanciacionGeneral();
        Juego_AplicarConfiguracion();
    }

    public static void SeleccionMenuSurvival()
    {
        FindObjectOfType<UI_Menu>().SelectorSurvival();
    }


    public void Juego_AplicarConfiguracion_Retrasada()
    {
        instance.StartCoroutine(Esperar(.5f));
        GameController.Juego_AplicarConfiguracion();

    }

    IEnumerator Esperar(float x)
    {
        yield return new WaitForSeconds(x);

    }

    public static void Juego_AplicarConfiguracion()
    {
        if (AplicarConfiguracion != null) AplicarConfiguracion();
    }

    


    public static void IniciarSesionDeJuego()
    {
        if (InicioSesionDeJuego != null) InicioSesionDeJuego();
    }

    public static void FinalizarSesionDeJuego()
    {
        if (FinSesionDeJuego != null) FinSesionDeJuego();
    }




    public static float GetTiempo()
    {
        return instance.tiempoDeJuego;
    }

    public static void CambiarModoDeJuego(modo x)
    {
        Debug.Log("Modo de juego Cambiado");
        if (instance.modoDeJuegoActivo == x) return;
        instance.modoDeJuegoActivo = x;
        
        if(x > modo.Menu && x < modo.Niveles)
        {
            instance.CargarEscenaSurvival();
        }

        GameController.instance.juego.Desenlazar();
    }

   

    public static GameObject InstanciarGameObject(string path, Transform t)
    {
        GameObject clone = new GameObject();
        if (t != null)
        clone = Instantiate(Resources.Load(path, typeof(GameObject)), t) as GameObject;
        else
        clone = Instantiate(Resources.Load(path, typeof(GameObject)), null) as GameObject;
        return clone;
    }

    public static GameObject InstanciarGameObject(string path, Vector3 pos, Quaternion rot)
    {
        return Instantiate(Resources.Load(path, typeof(GameObject)), pos, rot) as GameObject;
    }

    public void CargarModoDeJuego()
    {
        if (canvasActual != null)
        {
            UI_GameMode x = canvasActual.GetComponent<UI_GameMode>();
            if (x != null) x.Desenlazar();
            Destroy(canvasActual);
        }
        if (juego != null)
        {
            juego.Desenlazar();
            juego = null;
        }

        switch (modoDeJuegoActivo)
        {
            case modo.Menu:
                juego = AdministradorDeModos.instance.modo_Menu;
                CargarHome();
                break;
            case modo.Survival_Classic:
                juego = AdministradorDeModos.instance.modo_Classic;
                break;
            case modo.Survival_Restrained:
                juego = AdministradorDeModos.instance.modo_Restrained;
                break;
            case modo.Survival_Spike:
                juego = AdministradorDeModos.instance.modo_Spikes;
                break;
            case modo.Survival_TimeBomb:
                juego = AdministradorDeModos.instance.modo_TimeBomb;
                break;
            case modo.Survival_Rain:
                break;
            case modo.Survival_MoneyHunter:
                break;
            case modo.Survival_Twister:
                break;
            case modo.Niveles:
                break;
            default:
                break;
        }
        juego.Enlazar();
        //juego.CrearCanvas();
        //canvasActual = juego.GetCanvasActual();
    }

    public void CargarHome()
    {
        SelectorNivel.CargarNivel(0);
    }
    

    private void CargarEscenaSurvival()
    {
        if(canvasActual != null)
        {
            Destroy(canvasActual.gameObject);
        }
        SelectorNivel.CargarModoSurvival();
    }





    }
