﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControlV2 : MonoBehaviour, SesionDeJuego {

    public bool activado = false;

    public static TouchControlV2 instance;
    [SerializeField]public ConfiguracionDeBarras config;


    public RecursoBarra recurso;
    private ContenedorBarras contenedor;
    private struct idTouch
    {
        public int fingerId;
        public Vector2 posInicial;
        public Vector2 touchInicial;

        public idTouch(int fingerId, Vector2 posInicial, Vector2 tInicial)
        {
            this.fingerId = fingerId;
            this.posInicial = posInicial;
            this.touchInicial = tInicial;
        }
    }

    public delegate void Act(int i);    
    public static Act barraCreada;
    
    static private int barrasCreadas = 0;    
    static public int BarrasCreadas
    {
        get {
            return barrasCreadas; }
        set
        {
            if (barraCreada != null)
            {
                barraCreada(value);
            }
       //     Debug.Log("BARRA N" + value);
            barrasCreadas = value;
        }
    }   
    

    Vector2[] touchPosInicial = new Vector2[3];
    List<idTouch> listaTouchs = new List<idTouch>();

    private void Start()
    {
        if (instance == null) instance = this;
        Enlazar();
        InstanciacionGeneral();
    }

    public void InstanciacionGeneral()
    {
        if(GameController.instance.juego.configBarras != null)        config = GameController.instance.juego.configBarras;

        activado = true;
        if (recurso == null) recurso = new RecursoBarra(config.RecursoMax);
        else
        {
            recurso.SetMax(config.RecursoMax);
        }
        if (contenedor == null)
            contenedor = new ContenedorBarras(config.cantBarras, config.barraDesaparecePorGolpe);
        contenedor.AplicarConfiguracionBarras();
    }

    public void AplicarConfiguracion()
    {
        VaciarLista();
        Debug.Log("Barras reseteadas");
        BarrasCreadas = 0;
        contenedor.DeshabilitarBarras();
        instance.recurso.Recargar(instance.config.LongitudMax);
    }
    
 


    void VaciarLista()
    {
        listaTouchs.Clear();
    }

    public void SesionDeJuegoInterrumpida()
    {
        contenedor.DeshabilitarBarras();
    }

    public void DeshabilitarBarras()
    {
        contenedor.DeshabilitarBarras();
    }

    public float CalcularRotacionAyB(Vector2 posA, Vector2 posB)
    {
        Vector2 dif = posB - posA;                                                     
        return  Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;
    }

    public void Update()
    {
        if(GameController.enPausa && !GameController.swipeActivo)
        {
            return;
        }

#if UNITY_ANDROID
        CrearBarrasTouch();
#endif

#if UNITY_EDITOR
       CrearBarrasMouse();
#endif

    }
    
    void CrearBarrasTouch()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch t = Input.GetTouch(i);
            switch (t.phase)
            {
                case TouchPhase.Began:
                     touchIn(t.fingerId, t.position);
                    break;
                case TouchPhase.Moved:
                touchMove(t.fingerId, t.position);               
                    break;        
                case TouchPhase.Ended:
                touchEnd(t.fingerId, t.position);
                    break;
                case TouchPhase.Canceled:
                    //Extraer ID
                    break;
                default:
                    break;
            }
        }
    }

    void CrearBarrasMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            touchIn(0, Input.mousePosition);
        }
        else if (Input.GetMouseButton(0))
        {
            touchMove(0, Input.mousePosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            touchEnd(0, Input.mousePosition);
        }
    }

    private void touchIn(int fingerId, Vector2 tPos)
    {
        //Se guarda el fingerID en una lista + Posicion inicial + barra asignada
        idTouch t;
        t = new idTouch(fingerId, Camera.main.ScreenToWorldPoint(tPos) - new Vector3(0f, 0f, Camera.main.transform.position.z), tPos);
        listaTouchs.Add(t);
        contenedor.PosicionarReferencia(t.posInicial, Quaternion.identity,0, t.fingerId, true);
    }


    private void touchMove(int fingerId, Vector2 actPos)
    {
        bool vibrar = false;
        if (listaTouchs.Count == 0)
        {
            Debug.LogWarning("Lista vacia en Move");
            return;
        }
        //Obtener t de la lista        
        idTouch t = listaTouchs.Find(x => x.fingerId == fingerId);
        //Calculamos direccion y longitud de la barra
        Vector2 target = Camera.main.ScreenToWorldPoint(actPos);         //Punto del touch actual
        float dist;
        Vector2 dif;
        float angle;
        dist = Vector2.Distance(t.posInicial, target);                                        //Distancia entre actual e inicial
        if (dist > config.LongitudMax) dist = config.LongitudMax;                                              //Si el tamaño supera el maximo se limita al mismo


        float lim = config.LongitudMax;

      

        if (config.limitacion == ConfiguracionDeBarras.tipoLimitacion.RecursoSimple)
        {
            dist = recurso.SolicitarDisponibilidadRecurso(dist);
            lim = recurso.SolicitarDisponibilidadRecurso(lim);
        }
        lim = lim * .85f;
        if (dist > lim)
        {
            vibrar = true;
        }

        CalcularRotacionAyB(t.posInicial, target);
        dif = target - t.posInicial;                                                        //Dif de la rotacion
        angle = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;                                                    //Angulo de la rotacion
        contenedor.PosicionarReferencia(t.posInicial, Quaternion.AngleAxis(angle, Vector3.forward), dist, t.fingerId, false,vibrar);
    }


    private void touchEnd(int fingerId, Vector2 actPos)
    {
        if (listaTouchs.Count == 0)
        {
            Debug.LogWarning("Lista vacia en End");
            return;
        }
        //Obtener t de la lista
        idTouch t = listaTouchs.Find(x => x.fingerId == fingerId);
        listaTouchs.Remove(t);       //eliminamos de la lista
        //Calculamos direccion y longitud de la barra
        Vector2 target = Camera.main.ScreenToWorldPoint(actPos);         //Punto del touch actual      

        //calculamos distancia y si es menor al minimo cancelamos todo
        float dist = Vector2.Distance(t.posInicial, target);
        float angle = CalcularRotacionAyB(t.posInicial, target);
        if (dist > config.LongitudMin)
        {   
            
            if(dist > config.LongitudMax)  dist = config.LongitudMax;
            if (config.limitacion == ConfiguracionDeBarras.tipoLimitacion.RecursoSimple)         //Si utilizamos la longitud como un recurso esta se descuenta y reduce la cantidad actual
            {
                dist = recurso.SolicitarDisponibilidadRecurso(dist);
                recurso.Consumir(dist);
            }
            contenedor.PosicionarBarra(t.posInicial, Quaternion.AngleAxis(angle, Vector3.forward), dist, t.fingerId);
            BarrasCreadas++;
            Debug.Log("Barra Creada");
        }
    }


    private void OnDestroy()
    {
        Desenlazar();
    }

    public void InicioSesionDeJuego()
    {
        throw new NotImplementedException();
    }

    public void FinSesionDeJuego()
    {
        throw new NotImplementedException();
    }

    public void PausaIn()
    {
        throw new NotImplementedException();
    }

    public void PausaOut()
    {
        throw new NotImplementedException();
    }

    public void CierreDeEscena()
    {
        throw new NotImplementedException();
    }


    public void Enlazar()
    {

        GameController.InstanciacionGeneral += InstanciacionGeneral;
        GameController.AplicarConfiguracion += AplicarConfiguracion;
    }

    public void Desenlazar()
    {
        GameController.InstanciacionGeneral -= InstanciacionGeneral;
        GameController.AplicarConfiguracion -= AplicarConfiguracion;
    }

}
