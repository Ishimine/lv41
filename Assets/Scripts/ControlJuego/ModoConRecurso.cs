﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ModoConRecurso
{
    RecursoBarra GetRecurso();

    void RecargarRecurso();
    void GastarRecurso();



}
