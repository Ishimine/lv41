﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ConfiguracionDeBarras
{
    /*
    /// <summary>
    /// Infinito, cantidad ilimitada
    /// FinitoCircular, llega a un limite y empieza a reulizar las barras existentes, utiliza un contenedor de barras
    /// </summary>
    public enum tipoCreacion { Infinito, FinitoCircular};
    public tipoCreacion creacion;
    */

    /// <summary>
    /// Sin limitacion, no limita la creacion de barra
    /// RecursoSimple: Utiliza un pool de recurso
    /// RecursoActivo: Utiliza un pool de recurso activo que toma en cuenta las barras existentes
    /// </summary>
    public enum tipoLimitacion {SinLimitacion, RecursoSimple, RecursoActivo};
    public tipoLimitacion limitacion;

    public int cantBarras = 3;

    [Space(2)]

    [Header("Desaparicion de barra por:")]

    public bool tbarraDesaparecePorTiempo;
    public float tiempoVidaBarra = 3f;
    [Space(1)]
    public bool barraDesaparecePorGolpe;
    [Space(1)]
    public bool barraDesaparecePorDobleTap;

    [Space(5)]

    [Header("Limitaciones:")]

    private float longitudMax = 10;
    public float LongitudMax
    {
        get
        {
            return longitudMax;
        }
        set
        {
            longitudMax = value;
        }
    }
    private float longitudMin = .1f;
    public float LongitudMin
    {
        get
        {
            return longitudMin;
        }
        set
        {
            longitudMin = value;
        }
    }
    [Space(1)]
    private float recursoMax = 6;
    public float RecursoMax
    {
        get
        {
            return recursoMax;
        }
        set
        {
            recursoMax = value;
        }
    }
    [Space(1)]
    private float coeficienteDeRecuperacion = 1;
    public float CoeficienteDeRecuperacion
    {
        get
        {
            return coeficienteDeRecuperacion;
        }
        set
        {
            coeficienteDeRecuperacion = value;
        }
    }

       

    public float grosorBarra = 1;

    public bool permitirCrearMultiplesBarras = false;

    public bool usarMouse = false;



}
