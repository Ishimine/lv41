﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ContenedorBarras : MonoBehaviour {


    public GameObject prefab;
    public BarraTouch[] barras = new BarraTouch[0];

    public int cantidadBarras = 20;


    public bool puedeTocarBarrasTransparentes = false;

    private struct idTouch
    {
        public int fingerId;
        public int barraId;
        public Vector2 posInicial;
        public Vector2 touchInicial;

        public idTouch(int fingerId, int barraId, Vector2 posInicial, Vector2 tInicial)
        {
            this.fingerId = fingerId;
            this.barraId = barraId;
            this.posInicial = posInicial;
            this.touchInicial = tInicial;
        }
    }

    List<idTouch> listaT = new List<idTouch>();

    List<BarraTouch> barrasActivas = new List<BarraTouch>();


  

    public ContenedorBarras(int cant, bool usarDesaparicionAlTacto)
    {
        prefab = Resources.Load("Prefabs/BarraTouch") as GameObject;
        BarraTouch.UsarDesaparicionAlTacto = usarDesaparicionAlTacto;
        cantidadBarras = cant;
        Inicializar();
    }


    public void Inicializar()
    {
        CrearBarras();
    }

    public void CrearBarras()
    {
        EliminarBarras();
        barras = new BarraTouch[cantidadBarras];
        for (int i = 0; i < cantidadBarras; i++)
        {
            GameObject clone = Instantiate<GameObject>(prefab, GameController.instance.transform);
            barras[i] = clone.GetComponent<BarraTouch>();
            barras[i].IdBarra = i;
            barras[i].SetContenedor(this);
            barras[i].Deshabilitar();
            barras[i].SetGrosor(2);
        }
    }


    public void LimpiarIds()
    {
        foreach(BarraTouch b in barras)
        {
            b.IdTouch = -1;
        }
    }

    public void EliminarBarras()
    {
        Debug.LogWarning("Eliminando barras");

        BarraTouch[] a = FindObjectsOfType<BarraTouch>();

        for (int i=0; i< a.Length;i++)
        {
            Destroy(a[i].gameObject);
        }
    }

    public void DeshabilitarBarras()
    {
        Debug.Log("Deshabilitando barras");
        foreach(BarraTouch b in barras)
        {
            b.Deshabilitar();
        }
        LimpiarIds();
    }
    

    public void AplicarConfiguracionBarras()
    {
        Inicializar();
    }

    /// <summary>
    /// La barra del codigo "id" fue terminada y debe sacarse de la lista de creacion.
    /// </summary>
    /// <param name="id"></param>
    public void BarraFinalizada(int id)
    {
        Debug.Log("Barra Finalizada");
        BarraDeshabilitada(id);
    }


    /// <summary>
    /// La barra ya no existe y por ende se puede volver a utilizar sin necesidad de reutilizar una activa
    /// </summary>
    /// <param name="id"></param>
    public void BarraDeshabilitada(int id)
    {
        barrasActivas.Remove( barrasActivas.Find(x => x.IdBarra == id));
    }



    public BarraTouch PosicionarBarra(Vector2 pos, Quaternion rot, float longitud, int idTouch)
    {
        BarraTouch b = GetBarra(idTouch, false);
        if (b == null) return b;
        b.AplicarBarraFisica(pos, rot, longitud);
        barrasActivas.Add(b);
        return b;
    }

    /*
    public void PosicionarReferencia(Vector2 pos, Quaternion rot, float longitud, int idTouch, bool tInicial, bool vibrar =false)
    {
        BarraTouch b = PosicionarReferencia(pos, rot, longitud, idTouch, vibrar);
        if (b != null) b.vibrar = true;
    }*/

    public BarraTouch PosicionarReferencia(Vector2 pos, Quaternion rot, float longitud, int idTouch, bool tInicial, bool vibrar = false)
    {
        BarraTouch b = GetBarra(idTouch, tInicial);
        if (b == null) return b;
        b.SetTransparenteTransform(pos, rot, longitud);
        b.vibrar = vibrar;
        return b;
    }

    public BarraTouch GetBarra(int id, bool nueva)
    {
        BarraTouch b = null;

        b = GetBarraPorIdTouch(id);
        if (b == null && nueva)
        {
            if (barrasActivas.Count < cantidadBarras)   
            {
                b = GetBuscarBarraInactiva();
            }
            else
            {
                b = GetBarraMasVieja();
            }
            if (b == null)
            {

                return b;
            }
            b.IdTouch = id;
        }
        return b;
    }


    public BarraTouch GetBuscarBarraInactiva()
    {
        BarraTouch b = null;
        for(int i = 0; i < cantidadBarras; i++)
        {
            if (!barras[i].Activa)
            {
                b = barras[i];
                //Debug.Log("ENCONTRADA Barra inactiva N:" + i);
                break;
            }
        }
        return b;
    }

    public BarraTouch GetBarraMasVieja()
    {
        BarraTouch b = barrasActivas[0];
        barrasActivas.RemoveAt(0);
        return b;
    }

    public BarraTouch GetBarraPorIdTouch(int id)
    {
        foreach(BarraTouch b in barras)
        {
            if (b.IdTouch == id)
            {
                return b;
            }
        }
        return null;
    }

    private void OnDestroy()
    {
        EliminarBarras();
    }


}
