﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraJugadorFisica : MonoBehaviour {

    public BarraTouch barra;
    public Transform pivot;

    public Collider2D col;




    IEnumerator rutina;


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            barra.ContactoFisico();
        }
    }


    public void AnimarAparicion()
    {
        rutina = Aparicion();
        StartCoroutine(rutina);
    }


    public void AnimarDesaparicion()
    {
        rutina = Desaparicion();
        StartCoroutine(rutina);
    }


    IEnumerator Desaparicion()
    {
        yield return null;
    }

    IEnumerator Aparicion()
    {
        yield return null;
    }

    private void OnDestroy()
    {
        if (rutina != null) StopCoroutine(rutina);
    }
}
