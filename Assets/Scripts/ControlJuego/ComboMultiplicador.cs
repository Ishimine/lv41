﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboMultiplicador
{

    public delegate void ActualizacionFloat(float mAct);
    public delegate void ActualizacionInt(int mAct);


    public ActualizacionFloat actMult;
    public ActualizacionFloat actTiempo;
    public ActualizacionInt actPaso;


    bool activo = false;
    /// <summary>
    /// Tiempo maximo entre ingreso de puntos para seguir sumando
    /// </summary>
    private float tiempoMaxDeIngreso = 10;
    public float TiempoMaxDeIngreso
    {
        get { return tiempoMaxDeIngreso; }
        set { tiempoMaxDeIngreso = value; }
            //Debug.Log("tiempo de ingreso max seteado: " + value); }
    }


    float tiempoActual;
    float TiempoActual
    {
        get { return tiempoActual; }
        set {
            if (actTiempo != null) actTiempo(value);

            tiempoActual = value;
        } }

    /// <summary>
    /// Cantidad de pasos necesarios para subir de nivel el multiplicador
    /// </summary>
    int pasosNecesarios = 5;
    public int PasosNecesarios
    {
        get { return pasosNecesarios; }
        set
        {
            pasosNecesarios = value;
        }
    }


    int pasoActual=0;
    public int PasoActual
    {
        get { return pasoActual; }
        set
        {
            pasoActual = value;
            if (actPaso != null)
                actPaso(pasoActual);
        }
    }

    float multiplicadorAumento = 1;
    public float MultiplicadorAumento
    {
        get
        {
            return multiplicadorAumento;
        }
        set
        {
            multiplicadorAumento = value;
        }
    }


    float multiplicadorActual = 1;
    float MultiplicadorActual { get { return multiplicadorActual; }
        set { multiplicadorActual = value;
            if (actMult != null) actMult(multiplicadorActual);
        } }


    float multiplicadorMaximo = 10;
    public float MultiplicadorMaximo
    {
        get
        {
            return multiplicadorMaximo;
        }
        set
        {
            multiplicadorMaximo = value;
        }
    }


    public void Iniciar()
    {
        activo = true;
        tiempoActual = tiempoMaxDeIngreso;
    }

    public void Detener()
    {
        activo = false;
    }

    public float GetMultiplicadorActual() { return multiplicadorActual; }

    public void SetMultiplicadorActual(float value) { multiplicadorActual = value; }
 //   public void SetmultiplicadorAumento(float value) { multiplicadorAumento = value; }
    public void SetmultiplicadorMaximo(float value) { multiplicadorMaximo = value; }

    
    //public float GetTiempoDeIngreso() { return tiempoMaxDeIngreso; }
    //public void SetTiempoDeIngreso(float value) { tiempoMaxDeIngreso = value; }

    public void SetPasosNecesarios(int value) { pasosNecesarios = value; }
    public int GetPasosNecesarios() { return pasosNecesarios; }
    public void SetPasosActual(int value) { pasoActual = value; }
    public int GetPasosActual() { return pasoActual; }

    public ComboMultiplicador()
    {
        //EnlazarSistema();
        ResetearValores();
    }

    public ComboMultiplicador(float multiplicadorAumento, float multiplicadorActual, float multiplicadorMaximo,
        int pasosNecesarios, int pasoActual, float tiempoActual,float tiempoMaxDeIngreso)
    {
        //EnlazarSistema();
        this.multiplicadorAumento = multiplicadorAumento;
        this.MultiplicadorActual = multiplicadorActual;
        this.multiplicadorMaximo = multiplicadorMaximo;
        this.pasosNecesarios = pasosNecesarios;
        this.PasoActual = pasoActual;
        this.TiempoActual = tiempoActual;
        this.TiempoMaxDeIngreso = tiempoMaxDeIngreso;
    }

    public void ResetearValores()
    {

        multiplicadorAumento = 1;
        MultiplicadorActual = 1;
        MultiplicadorMaximo = 10;
        pasosNecesarios = 5;
        PasoActual = 0;
        TiempoActual = 0;
        TiempoMaxDeIngreso = 10;

    }

    public void EnlazarSistema()
    {
        GameController.gcUpdate += Update;

    }

    /// <summary>
    /// Recibe puntaje y calcula el valor de salida en relacion al nivel actual de multiplicador. Tambien suma 1 paso al contador de pasos
    /// </summary>
    /// <param name="value"></param>
    public float IngresoDePuntaje(float value)
    {

        PasoActual++;

   //     Debug.Log("Tiempo Restante: " + TiempoActual);

        if (TiempoActual > 0)
        {
            if (PasoActual >= pasosNecesarios)
            {
                AumentarMultiplicador();
          


               // Debug.Log("(" + pasoActual + "/" + pasosNecesarios + ") " + " 1 x " + MultiplicadorActual + "=");

                PasoActual = 0;
            }     /*      
            else
            {
                Debug.Log("(" + pasoActual + "/" + pasosNecesarios + ") " + " 1 x " + MultiplicadorActual + "=");
            }*/
        }


        value *= MultiplicadorActual;

   //     Debug.Log(value);


        TiempoActual = tiempoMaxDeIngreso;


        activo = true;
        return value;
    }


    public void AumentarMultiplicador()
    {
        MultiplicadorActual += multiplicadorAumento;
        if (MultiplicadorActual >= multiplicadorMaximo) MultiplicadorActual = multiplicadorMaximo;

        TiempoMaxDeIngreso -= (TiempoMaxDeIngreso / 10);
        Debug.Log("TIempo Max De Ingreso reducido a: " + TiempoMaxDeIngreso);

        CreadorDeMonedas.instance.tiempoPorMonedaAct -= (CreadorDeMonedas.instance.tiempoPorMonedaAct / 10);
        Debug.Log("tiempoPorMonedaAct Reducido a: " + CreadorDeMonedas.instance.tiempoPorMonedaAct);
        

    }

    public void ComboBreaker()
    {
      //  Debug.Log("D:");
        TiempoActual = 0;
        PasoActual = 0;
        MultiplicadorActual = 1;
        activo = false;
    }

    public void Update(float deltaTime)
    {
        if (!activo) return;

        TiempoActual -= deltaTime;
        if (TiempoActual < 0)
        {
            ComboBreaker();            
        }
    }


    /// <summary>
    /// Es necesario ejecutarlo antes de eliminar el objeto
    /// </summary>
    public void DesenlazarSistema()
    {
        GameController.gcUpdate -= Update;
    }


}
