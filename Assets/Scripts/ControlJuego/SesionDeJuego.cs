﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface SesionDeJuego
{

 

    /// <summary>
    /// Creacion/Instanciacion de Sistemas/Canvas/Objetos necesarios para la escena
    /// </summary>
    void InstanciacionGeneral();
    /// <summary>
    /// Se inicializan valores de acuerdo con el modo de juego seleccionado
    /// </summary>
    void AplicarConfiguracion();
    /// <summary>
    /// Donde se activan los sistemas de juegos, se inicia el timescale y por ende la fisica 
    /// Time.Timescale se iguala a 0
    /// </summary>
    void InicioSesionDeJuego();
    /// <summary>
    /// Se DETIENEN totalmente los sistemas de juego, se evaluan puntajes y se almacenan
    /// Time.Timescale se iguala a 1
    /// </summary>
    void FinSesionDeJuego();
    /// <summary>
    /// Se detienen/eliminan totalmente sistemas innecesarios para otros modos de juego.
    /// Los elementos propios del modo de juego se destruyen voluntariamente(escencial para canvas y elementos opcionales)
    /// </summary>
    void CierreDeEscena();

    /// <summary>
    /// Se PAUSAN los sistemas de juego
    /// Time.Timescale se iguala a 0
    /// </summary>
    void PausaIn();
    /// <summary>
    /// Se DESPAUSAN los sistemas de juego
    /// Time.Timescale se iguala a 1
    /// </summary>
    void PausaOut();
  


    /// <summary>
    /// Aca se deberian ENLAZAR los varios metodos con sus eventos contrapartes en el GAMECONTROLLER
    /// </summary>
    void Enlazar();

    /// <summary>
    /// Aca se deberian DESENLAZAR los varios metodos con sus eventos contrapartes en el GAMECONTROLLER
    /// </summary>
    void Desenlazar();
}
