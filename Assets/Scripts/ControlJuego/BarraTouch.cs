﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class BarraTouch : MonoBehaviour {

    public bool vibrar = false;
    public float vel = 6;
    public float porcentajeCrecimientoX = .1f;
    public float porcentajeCrecimientoY = .2f;


    ContenedorBarras contenedor;

    public bool contactoTransparente = false;

    private bool activa;


    public bool Activa
    {
        get { return activa; }
    }


    private static bool usarDesaparicionAlTacto;
    public static bool UsarDesaparicionAlTacto
    {
        get { return usarDesaparicionAlTacto; }
        set { usarDesaparicionAlTacto = value; }
    }


    public static bool usarTiempoDeVida = false;
    public static float tiempoDeVida = 3;

    private int id;
    public int IdBarra
    {
        get { return id; }
        set { id = value; }
    }


    private int idTouch = -1;
    public int IdTouch
    {
        get { return idTouch; }
        set { idTouch = value; }
    }


    public BarraJugadorFisica pFisica;
    public BarraJugadorTransparente pTransparente;

    private void Awake()
    {
        if (contactoTransparente)
            pTransparente.SetContacto(true);
    }

    public void LateUpdate()
    {
        if(vibrar)
        {
            float x = Mathf.Abs((Mathf.Sin(Time.realtimeSinceStartup * vel) * porcentajeCrecimientoX));
            float y = Mathf.Abs((Mathf.Sin(Time.realtimeSinceStartup * vel) * porcentajeCrecimientoY));
            pTransparente.transform.localScale = new Vector3(x + 1, y + 1);
        }
    }

    public void SetContenedor(ContenedorBarras c)
    {
        contenedor = c;
    }
    
    public void SetTransparenteIsTrigger(bool x)
    {
        pTransparente.SetContacto(x);
    }

    public void SetTransparenteTransform(Vector2 pos, Quaternion rot, float longitud)
    {
        SetTransparenteTransform(pos, rot);
        SetTransparenteLongitud(longitud);
    }

    public void SetTransparenteTransform(Vector2 pos, Quaternion rot)
    {
        SetTransparentePosicion(pos);
        SetTransparenteRotacion(rot);
    }

    public void SetTransparenteLongitud(float longitud)
    {
        pTransparente.pivot.transform.localScale = new Vector2(longitud, pTransparente.pivot.transform.localScale.y);
    }

    public void SetTransparentePosicion(Vector3 pos)
    {
        pTransparente.gameObject.SetActive(true);
        pTransparente.pivot.transform.position = pos;
    }

    public void SetTransparenteRotacion(Quaternion rot)
    {
        pTransparente.pivot.transform.rotation = rot;
    }    

    public void SetGrosor(float grosor)
    {
        pFisica.pivot.transform.localScale = new Vector2(pFisica.pivot.transform.localScale.x, grosor);
        pTransparente.pivot.transform.localScale = new Vector2(pTransparente.pivot.transform.localScale.x, grosor);
    }



    /// <summary>
    /// Finaliza la creacion de la barra Fisica usando la configuracion de la barra transparente.
    /// DEPENDENCIA: La barra transparente solo puede activar este metodo si su varialbe "Contacto" es verdadera
    /// </summary>
    public void ContactoTransparente()
    {
        AplicarBarraFisica(pTransparente.transform.position, pTransparente.transform.rotation, pTransparente.transform.localScale.x);
        contenedor.BarraFinalizada(IdBarra);
    }

    public void ContactoFisico()
    {
        if(usarDesaparicionAlTacto)
        {
            pFisica.gameObject.SetActive(false);
            contenedor.BarraDeshabilitada(id);
            Deshabilitar();
        }
    }

    public void AplicarBarraFisica(Vector3 pos, Quaternion rot, float longitud)
    {
        pTransparente.gameObject.SetActive(false);
        activa = true;
        pFisica.gameObject.SetActive(true);
        pFisica.pivot.position = pos;
        pFisica.pivot.rotation = rot;
        pFisica.pivot.localScale = new Vector2(longitud, pFisica.pivot.localScale.y);
        IdTouch = -1;



    }



    public void Deshabilitar()
    {
        activa = false;
        pFisica.gameObject.SetActive(false);
    }


}
