﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ModoConMinas
{
    CreadorDeMinas GetCreadorDeMinas();
    void MinaGolpeada(GameObject obj);

    void PanelMonedaEspecialAtrapado();
    void PanelMonedaEspecialPerdido();


}
