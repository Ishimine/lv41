﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscenarioMenu : MonoBehaviour {

    
    public enum Puerta{Derecha, Izquierda}

    public GameController.modo[] modos;


    public TextMesh[] textos; 

    public GameObject[] puertas;

    public GameObject[] flechas;



    public void Bloquear(Puerta x, string nombre)
    {
        textos[(int)x].text = nombre;
        Bloquear(x);
    }

    public void Bloquear(Puerta x)
    {
        puertas[(int)x].SetActive(true);
        flechas[(int)x].SetActive(false);
    }

    public void ModoVacio(Puerta x)
    {
        puertas[(int)x].SetActive(true);
        textos[(int)x].text = "";
        flechas[(int)x].SetActive(false);
    }

    public void AplicarModo(Puerta x, RequisitosDeModo modo)
    {
        //Debug.Log("Modo: " + modo.modo.ToString());


        modos[(int)x] = modo.modoReferencia;
        puertas[(int)x].SetActive(!modo.desbloqueado);
        textos[(int)x].text = modo.nombreOficial;
        flechas[(int)x].SetActive(modo.desbloqueado);


        if (modo.desbloqueado)
        {
            textos[(int)x].color = Color.white;
        }
        else
        {
            textos[(int)x].color = new Color(.5f, .5f, .5f,1);
        }
    }

    public void ActivarModo(Puerta x)
    {
        GameController.CambiarModoDeJuego(modos[(int)x]);
    }

    public void Desbloquear(Puerta x, string nombre)
    {
        textos[(int)x].text = nombre;
        Desbloquear(x);
    }

    public void Desbloquear(Puerta x)
    {
        flechas[(int)x].SetActive(true);
        puertas[(int)x].SetActive(false);
    }
}
