﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Boton_Sonido : BotonBase {

    public AudioMixer mixer;
    public bool prendido;


    private void Start()
    {
        CheckEstado();
    }

    public void CheckEstado()
    {
        if (!prendido)
        {
            mixer.SetFloat("VolMaestro", -80);
        }
        else
        {
            mixer.SetFloat("VolMaestro", 0);
        }

        if (prendido)
        {
            CambiarEstado(estados.Activado);
        }
        else
        {
            CambiarEstado(estados.Desactivado);
        }
    }


    public override void BotonApretado()
    {
        if(prendido)
        {
            mixer.SetFloat("VolMaestro", -80);
        }
        else
        {
            mixer.SetFloat("VolMaestro", 0);
        }
        prendido = !prendido;

        if (prendido)
        {
            CambiarEstado(estados.Activado);
        }
        else
        {
            CambiarEstado(estados.Desactivado);
        }
    }

}
