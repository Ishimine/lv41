﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaActivarModo : MonoBehaviour {

    public EscenarioMenu escMenu;
    public EscenarioMenu.Puerta puerta;

    public void ActivarModo()
    {
        escMenu.ActivarModo(puerta);
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            ActivarModo();
            Destroy(collision.gameObject);
        }
    }
}
