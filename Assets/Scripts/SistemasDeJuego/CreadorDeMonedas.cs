﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreadorDeMonedas : MonoBehaviour, SesionDeJuego {

    int contM;
    public GameObject prefab;
    public GameObject referenciaInicial;
    public TargetStarAnim targetAnim;

    public float distBorde = 1 ;
 [SerializeField]   private Vector2 dimPantalla;
    public Vector2 dimensiones = new Vector2(7,11);
    public Vector2 posicion = Vector2.zero;


    public Vector2 proxMonedaPos;
    public Vector2 posicionPrimerMoneda = new Vector2(0, 8);

    float distMinimaEntreMonedas = 5;

    public float tiempoPorMonedaAct = 4;

    public int valorMoneda = 10;

    public int valorMonedaPerdida = 15;
    public static CreadorDeMonedas instance;



    public bool usarMarcador = false;
    //public bool esperarSeñal = false;
    public int monedasAtrapadas = 0;
    public int monedasPerdidas = 0;
    public int penalizacionPorBarra = 20;
    public bool descontarMonedasPerdidas;
 //   public bool usarMonedasConTiempo = false;




    private float tiempoEntreMonedas;
    public float TiempoEntreMonedas
    {
        set 
{
            //Debug.Log("tiempoEntreMonedas: " + value);
            tiempoEntreMonedas = value; }
        get { return tiempoEntreMonedas; }
    }



    IEnumerator rutinaCrearMoneda;

    /*
    private int puntaje = 0;
    public int Puntaje
    {
        get { return puntaje; }
        set
        {
            puntaje = value;
            if (actPuntaje != null) actPuntaje(value);
            AdministradorPuntaje.instance.SetPuntaje(idPuntaje, value);

        }
    }*/

    MarcadorMoneda marcador;
    public delegate void trigger(int t);
    public trigger actPuntaje;


    private string path;
    /// <summary>
    /// Que id de puntaje representan las monedas. Por convension 0 = barras/moneda, 1 = muertes, 3=tiempo
    /// </summary>
    public int idPuntaje;




    private void Awake()
    {
        if(instance == null)
        {
            //prefab = Resources.Load(path)as GameObject;
                
            Enlazar();
            Vector2 dimPantalla = new Vector2(Camera.main.orthographicSize / Screen.height * Screen.width, Camera.main.orthographicSize);

            dimensiones = new Vector2(dimPantalla.x - distBorde, dimPantalla.y - distBorde);

            marcador = GameController.InstanciarGameObject("PickUp/MarcadorMoneda", proxMonedaPos, Quaternion.identity).GetComponent<MarcadorMoneda>();
            UsarMonedasConTiempo(true);
            instance = this;




        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, .92f, .16f, .5f);
        Gizmos.DrawCube(posicion, dimensiones*2);
    }

    public void UsarMonedasConTiempo(bool x)
    {
        if (x)
            path = "PickUp/MonedaConTiempo";
        else
            path = "PickUp/Moneda";
    }
    
    public void SetReferenciaInicialActive(bool x)
    {
        Debug.Log("Referencia cambiada: " + x);
        referenciaInicial.SetActive(x);
    }

    public void IniciarCreacionMonedas()
    {
        Debug.Log("Creacion de moneda iniciada ....................");
        proxMonedaPos = posicionPrimerMoneda;
        referenciaInicial.transform.position = posicionPrimerMoneda;
        targetAnim.gameObject.transform.position = posicionPrimerMoneda;
        targetAnim.Iniciar();


        StopAllCoroutines();
        if (rutinaCrearMoneda != null) StopCoroutine(rutinaCrearMoneda);


        //rutinaCrearMoneda = EsperarYCrearMoneda(TiempoEntreMonedas);
        //   Debug.Log("tiempoEntreMonedas: " + tiempoEntreMonedas);
        // StartCoroutine(rutinaCrearMoneda);
        CrearMoneda();


    }

    public void SetValorMoneda(int x)
    {
        valorMoneda = x;
    }

    public void SetValorMonedaPerdidad(int x)
    {
        valorMonedaPerdida = x;
    }

    public void SetTiempoPorMoneda(int x)
    {
        tiempoPorMonedaAct = x;
    }

    public void PenalizacionBarra()
    {
        AdministradorPuntaje.instance.RestaPenalizacionPuntaje(0,1);
    }

    public void Resetear()
    {
        //DestruirMonedas();
        proxMonedaPos = posicionPrimerMoneda;

        monedasAtrapadas = 0;
        monedasPerdidas = 0;
    }

    public void DestruirMonedas()
    {
        StopAllCoroutines();
        Moneda[] m = FindObjectsOfType<Moneda>();

        foreach (Moneda n in m)
        {
            Destroy(n.gameObject);
        }
    }

    public void CrearMoneda()
    {
        Debug.Log("CREADNO MONENDDAAAAAAA");
        contM++;
        GameObject clone = Instantiate<GameObject>(prefab,null);
        clone.transform.position = proxMonedaPos;

        clone.name = "Moneda N" + contM + " CREADA";
        Vector2 auxPos;
        do
        {
            auxPos = new Vector2(Random.Range(-dimensiones.x, dimensiones.x), Random.Range(-dimensiones.y, dimensiones.y));

        } while (Vector2.Distance(auxPos, proxMonedaPos) < distMinimaEntreMonedas);


        proxMonedaPos = auxPos;


        if (usarMarcador)
        {
            marcador.transform.position = proxMonedaPos;
            marcador.Activar(tiempoPorMonedaAct);
        }
        Debug.Log(clone);
        Debug.Log(clone.transform.position);
    }

    /*
    public void CortarYCrearMoneda()
    {
        if(rutinaCrearMoneda != null) StopCoroutine(rutinaCrearMoneda);
        rutinaCrearMoneda = EsperarYCrearMoneda(TiempoEntreMonedas);
        StartCoroutine(rutinaCrearMoneda);
    }*/

    public void MonedaAtrapada()
    {
        monedasAtrapadas++;
        CrearMoneda();
        //if (rutinaCrearMoneda != null) StopCoroutine(rutinaCrearMoneda);
        //   rutinaCrearMoneda = EsperarYCrearMoneda(TiempoEntreMonedas);
        // StartCoroutine(rutinaCrearMoneda);
    }

    public void MonedaPerdida()
    {
        monedasPerdidas++;
        CrearMoneda();
    /*    if (descontarMonedasPerdidas)
        {
            int x = AdministradorPuntaje.instance.valorPenalizacion[0];            
            AdministradorPuntaje.instance.RestaPenalizacionPuntaje(0, 0);
        }*/


    }

    public void DetenerCreacionMoneds()
    {
        if (rutinaCrearMoneda != null) StopCoroutine(rutinaCrearMoneda);
     //   DestruirMonedas();
    }

    public void InstanciacionGeneral()
    {
    }

    public void AplicarConfiguracion()
    {
        DestruirMonedas();
        marcador.transform.position = new Vector3(0, 0, -50);
        targetAnim.gameObject.transform.position = posicionPrimerMoneda;
        targetAnim.Iniciar();
        IniciarCreacionMonedas();
    }

    public void InicioSesionDeJuego()
    {
    }

    public void FinSesionDeJuego()
    {
      /*  Moneda[] monedas = FindObjectsOfType<Moneda>();

        foreach (Moneda m in monedas)
        {
            Destroy(m.gameObject);
        }*/
    }

    public void CierreDeEscena()
    {
    }

    public void PausaIn()
    {
    }

    public void PausaOut()
    {
    }

    public void Enlazar()
    {

        GameController.InstanciacionGeneral += InstanciacionGeneral;
        GameController.AplicarConfiguracion += AplicarConfiguracion;
        GameController.InicioSesionDeJuego += InicioSesionDeJuego;
        GameController.FinSesionDeJuego += FinSesionDeJuego;
        GameController.CierreDeEscena += CierreDeEscena;

    }

    public void Desenlazar()
    {
        GameController.InstanciacionGeneral -= InstanciacionGeneral;
        GameController.AplicarConfiguracion -= AplicarConfiguracion;
        GameController.InicioSesionDeJuego -= InicioSesionDeJuego;
        GameController.FinSesionDeJuego -= FinSesionDeJuego;
        GameController.CierreDeEscena -= CierreDeEscena;

    }

    private void OnDestroy()
    {
        Desenlazar();
    }
}
