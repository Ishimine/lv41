﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AreaDeSeleccion : MonoBehaviour {
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            Accion();
            Destroy(other.gameObject);
        }        
    }


   public abstract void Accion();

}
