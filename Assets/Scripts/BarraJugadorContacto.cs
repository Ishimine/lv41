﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraJugadorContacto : SonidoAlContacto {

    public Collider2D col;
    public AudioClip sfx;
    public BarraJugador barra;


    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (collision.gameObject.tag == "Player")
        {
            source.clip = sfx;
            source.Play();
            //col.enabled = false;
            barra.StopCoroutine(barra.rutina);

            if (TouchControl.instance.barrasVida == TouchControl.barrasSys.Clásico)
            {
                gameObject.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
    }


    public void OnDestroy()
    {
        if (TouchControl.instance.UsarLongitudComoRecurso && TouchControl.instance.barraDevuelveCosto)
        {
            TouchControl.instance.RecargarBarra(transform.parent.localScale.x);
        }
    }
}
