﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parpadeo : MonoBehaviour {

    public SpriteRenderer render;
    public float frecuencia = 2;
    public float offset = 2;

    public bool suavisado;


    private void Awake()
    {
        if(render == null)
        {
            render = GetComponent<SpriteRenderer>();
        }
    }

    void Update ()
    {
        float x = 0;
        if (suavisado)
        {
            x = Mathf.PingPong(Time.realtimeSinceStartup * frecuencia + offset, 1);
        }
        else
        {
            x = Time.realtimeSinceStartup % frecuencia+offset;
            if (x > .5f)
                x = 1;
            else
                x = 0;

        }
        render.color = new Color(render.color.r, render.color.g, render.color.b, x);
    }
}
