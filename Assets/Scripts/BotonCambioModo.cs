﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonCambioModo : MonoBehaviour {

    public GameController.modo modo;

    public void Home()
    {
        GameController.CambiarModoDeJuego(GameController.modo.Menu);
    }

    public void Modo()
    {
        GameController.CambiarModoDeJuego(modo);
    }

    public void ReiniciarModoNormal()
    {
        GameController.ReiniciarNivelNormal();
    }

    
}
