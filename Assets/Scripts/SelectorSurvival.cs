﻿using System.Collections;

using System.Collections.Generic;
using UnityEngine;

public class SelectorSurvival : MonoBehaviour {
    
    public PantallaDeModo[] pantallasModos;
    public int modoActual = 0;
   

    public void Siguiente()
    {
        SetPantallaActive(modoActual, false);
        modoActual = modoActual + 1;
        if(modoActual >= pantallasModos.Length)
        {
            modoActual = 0;
        }
        SetPantallaActive(modoActual, true);
    }

    public void Anterior()
    {
        SetPantallaActive(modoActual, false);
        modoActual = modoActual - 1;
        if (modoActual < 0)
        {
            modoActual = 0;
        }
        SetPantallaActive(modoActual, true);
    }

    private void SetPantallaActive(int id, bool x)
    {
        pantallasModos[id].gameObject.SetActive(x);
        pantallasModos[id].ChequearEstado();
    }

    public void CargarModo()
    {
        if(AdministradorDeModos.GetEstado(pantallasModos[modoActual].modo))
        {
            GameController.CambiarModoDeJuego(pantallasModos[modoActual].modo);
        }
    }

}
