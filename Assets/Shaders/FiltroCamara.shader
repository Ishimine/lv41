﻿Shader "Hidden/FiltroCamara"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Tint ("Color", Color) = (1,1,1,1)

		_AlphaExtra ("ExtraAlpha", Range(1,2)) = 0

		_Offset ("Offset", Range(0,6)) = 0




	
	}


	SubShader
	{
		Blend SrcAlpha OneMinusSrcAlpha

		//Blend SrcAlpha OneMinusSrcAlpha
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _Offset;

			float _AlphaExtra;
			half4 _Tint;




			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				//col = 1 - col;

				float aux_r = col.r;
				float aux_g = col.g;
				float aux_b = col.b;
				
				 
				

				float m = _Offset * 2;
				float n = (_Offset-.5) *2;
				
				
				n=sin(n);
				m=sin(m);
				
				if(n<0) n = 0;
				if(m<0) m = 0;
				
				
				if(n>1) n = 1;				
				if(m>1) m = 1;			

				col.g = (col.g - (col.g * m) + aux_b * m ) ;
				col.b = (col.b - (col.b * m) + aux_r * m ) ;
				col.r = (col.r - (col.r * m) + aux_g * m ) ;			

				
				aux_r = col.r;
				aux_g = col.g;
				aux_b = col.b;
				
				col.g = (col.g - (col.g * n) + aux_b * n);
				col.b = (col.b - (col.b * n) + aux_r * n);
				col.r = (col.r - (col.r * n) + aux_g * n);

				aux_r = col.a;

				float div = _AlphaExtra-1;

				if(col.r > col.b && col.r > col.g)
				{
					col.r = col.r * _AlphaExtra;
					col.g = col.g - (col.g * div);
				//	col.b = col.b - (col.b * div);
				}
				else if(col.g > col.b && col.g > col.r)
				{
					col.g = col.g * _AlphaExtra;
					col.r = col.r - (col.r * div);
				//	col.b = col.b - (col.b * div);
				}
				else if(col.b > col.r && col.b > col.g)
				{
					col.b = col.b * _AlphaExtra;
				//	col.g = col.g - (col.g * div);
					col.r = col.r - (col.r * div);
				}

				col = sin(col);
				col.a = aux_r;

				return col;
			}
			ENDCG
		}
	}
}

