﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AdministradorPuntaje))]
public class AdministradorPuntajeEditor : Editor
{

    public override void OnInspectorGUI()
    {
        AdministradorPuntaje myTarget = (AdministradorPuntaje)target;
        if (GUILayout.Button("Guardar Puntaje Actual"))
        {
            myTarget.GuardarPuntajeActual();
        }
        if (GUILayout.Button("Cargar Records Actual"))
        {
            myTarget.CargarDatosDeNivel();
        }




        if (GUILayout.Button("Guardar Records"))
        {
            myTarget.GuardarRecordsNiveles();  
        }        
        if (GUILayout.Button("Cargar Records"))
        {
            myTarget.CargarRecordsNiveles();
        }


        if (GUILayout.Button("Guardar Objetivos"))
        {
            myTarget.GuardarObjetivosNiveles();
        }
        if (GUILayout.Button("Cargar Objetivos"))
        {
            myTarget.CargarObjetivosNiveles();
        }



        if (GUILayout.Button("Resetear Records"))
        {
            myTarget.ResetRecords();
        }

        DrawDefaultInspector();

    }
}
