﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AdministradorDeModos))]
public class AdministradorDeModosEditor : Editor {

    public override void OnInspectorGUI()
    {


        if (GUILayout.Button("Guardar Requisitos"))
        {
            AdministradorDeModos.GuardarRequisitos();
        }
        if (GUILayout.Button("Cargar Requisitos"))
        {
            AdministradorDeModos.CargarRequisitos();
        }
        
        DrawDefaultInspector();
    }
}
